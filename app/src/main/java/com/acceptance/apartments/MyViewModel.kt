package com.acceptance.apartments

import android.app.Application
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import androidx.lifecycle.AndroidViewModel
import com.acceptance.apartments.model.DaoApartmentEntity
import com.acceptance.apartments.model.DaoRemarkEntity
import com.acceptance.apartments.model.RemarkGroup

class MyViewModel(application: Application) : AndroidViewModel(application) {

    /**/
    lateinit var listRemarkFromJson: MutableMap<String, MutableList<RemarkGroup>>
    lateinit var listRemarkFromJsonCommerce: MutableMap<String, MutableList<RemarkGroup>>
    lateinit var listRemarkFromJsonParking: MutableMap<String, MutableList<RemarkGroup>>

    var newApartmentEntity = DaoApartmentEntity()

    var isCustomRemark: Boolean? = false

    var timeOnReview = 0L

    var timeOnStartReview = 0L

    var newRemark = DaoRemarkEntity()

    var isComplete50PercentTime = false
    var youLast10Min = false
    var isCompleteTime = false

    /**/

    var backGroundFragment: Pair<Drawable, Int> = Pair(
        application.getDrawable(R.drawable.parking),
        ContextCompat.getColor(application.baseContext, R.color.colorParkingBlue)
    )

    lateinit var activity: MainActivity

}