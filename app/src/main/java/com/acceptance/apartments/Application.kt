package com.acceptance.apartments

import android.app.Application
import com.acceptance.apartments.model.Repository

class Application : Application() {
    override fun onCreate() {
        super.onCreate()
        Repository.initRepository(this)
    }
}