package com.acceptance.apartments.model

data class SectionHelp(
    var section: String?,
    var items: MutableList<ItemsHelp>?
)