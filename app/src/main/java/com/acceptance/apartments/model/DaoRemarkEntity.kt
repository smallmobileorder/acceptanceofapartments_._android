package com.acceptance.apartments.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "DaoRemarkDB")
data class DaoRemarkEntity(
    @PrimaryKey(autoGenerate = true) var id: Long? = null,
    @ColumnInfo(name = "idApartment") var apartmentID: Long? = null,
    @ColumnInfo(name = "typeRoom") var typeRoom: String = "",
    @ColumnInfo(name = "typeRemark") var typeRemark: String = "",
    @ColumnInfo(name = "nameRemark") var nameRemark: String = "",
    @ColumnInfo(name = "isComplete") var isComplete: Boolean? = null,
    @ColumnInfo(name = "isShow") var isShow: Boolean = true,
    @ColumnInfo(name = "levelCritic") var levelCritic: Int = 0,
    @ColumnInfo(name = "actionIfExist") var actionIfExist: String = "",
    @ColumnInfo(name = "docs") var docs: String = "",
    @ColumnInfo(name = "photo") var photo: String = "",
    @ColumnInfo(name = "comment") var comment: String = ""
)