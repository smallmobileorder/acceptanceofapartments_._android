package com.acceptance.apartments.model

data class ItemsHelp(
    var text: String?,
    var images: Images?
)
