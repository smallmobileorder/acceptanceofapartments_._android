package com.acceptance.apartments.model

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


class Converters {

    @TypeConverter
    fun fromReviews(value: MutableList<String>?): String? {
        return Gson().toJson(value)
    }

    @TypeConverter
    fun toReviews(value: String?): MutableList<String>? {
        val objects = Gson().fromJson(value, Array<String>::class.java) as Array<String>
        return objects.toMutableList()
    }

    @TypeConverter
    fun fromProgress(value: MutableSet<Remark>?): String? {

        return Gson().toJson(value?.toMutableSet())
    }

    @TypeConverter
    fun toProgress(value: String?): MutableSet<Remark>? {
        val objects = Gson().fromJson(value, Array<Remark>::class.java) as Array<Remark>
        return objects.toMutableSet()
    }


    @TypeConverter
    fun fromPhoto(value: MutableMap<String, MutableList<RemarkGroup>>?): String? {
        return Gson().toJson(value)
    }

    @TypeConverter
    fun toPhoto(value: String?): MutableMap<String, MutableList<RemarkGroup>>? {
        val mapType = object : TypeToken<Map<String, MutableList<RemarkGroup>>>() {}.type
        val son = Gson().fromJson(value, mapType) as Map<String, MutableList<RemarkGroup>>
        return son.toMutableMap()
    }

    @TypeConverter
    fun fromEnam(value: TypeApartment?): String? {
        return value?.name
    }

    @TypeConverter
    fun toEnam(value: String?): TypeApartment? {
        return TypeApartment.valueOf(value!!)
    }

    @TypeConverter
    fun fromEnamReview(value: TypeReview?): String? {
        return value?.name
    }

    @TypeConverter
    fun toEnamReview(value: String?): TypeReview? {
        if (value == null) return null
        return TypeReview.valueOf(value)
    }
}