package com.acceptance.apartments.model

import androidx.room.*

@Dao
interface DaoInterfaceRemark {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRemark(remark: DaoRemarkEntity)

    @Update
    fun updateRemark(remark: DaoRemarkEntity)

    @Delete
    fun deleteRemark(remark: DaoRemarkEntity)

    @Query("SELECT * FROM DaoRemarkDB")
    fun getAllRemarkList(): List<DaoRemarkEntity>

    @Query("SELECT * FROM DaoRemarkDB WHERE idApartment == :apartmentID")
    fun getRemarkListFromApartment(apartmentID: Long): List<DaoRemarkEntity>

    @Query("SELECT * FROM DaoRemarkDB WHERE idApartment == :apartmentID AND typeRoom == :typeRoom")
    fun getRemarkListFromApartmentFromRoom(apartmentID: Long, typeRoom: String): List<DaoRemarkEntity>

}