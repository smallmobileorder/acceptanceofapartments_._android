package com.acceptance.apartments.model

import android.annotation.SuppressLint
import android.content.Context
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@SuppressLint("StaticFieldLeak")
object Repository {

    private lateinit var context: Context

    fun clearDB(){
        AppDatabase.INSTANCE!!.clearAllTables()
    }

    fun initRepository(context: Context) {
        this.context = context
        AppDatabase.getAppDataBase(Repository.context)!!
    }

    fun setApartment(apartment: DaoApartmentEntity) {
        AppDatabase.getAppDataBase(context)!!.apartmentDao().insertApartment(apartment)
    }

    fun updateApartment(apartment: DaoApartmentEntity) {
        AppDatabase.getAppDataBase(context)!!.apartmentDao().updateApartment(apartment)
    }

    fun deleteApartment(apartment: DaoApartmentEntity) {
        AppDatabase.getAppDataBase(context)?.apartmentDao()?.deleteApartment(apartment)

    }


    fun setRemark(remark: DaoRemarkEntity) {
        AppDatabase.getAppDataBase(context)?.remarkDao()?.insertRemark(remark)
    }

    fun updateRemark(remark: DaoRemarkEntity) {
        AppDatabase.getAppDataBase(context)?.remarkDao()?.updateRemark(remark)
    }

    fun getApartmentList(callback: (apartmentList: List<DaoApartmentEntity>) -> Unit) {
        callback(AppDatabase.getAppDataBase(context)!!.apartmentDao().getAllApartmentList())
    }

    fun getApartment(id: Long, callback: (apartment: DaoApartmentEntity?) -> Unit) {
        callback(AppDatabase.getAppDataBase(context)!!.apartmentDao().getApartment(id))
    }

    fun getRemarkList(callback: (remark: DaoRemarkEntity) -> Unit) {
        AppDatabase.getAppDataBase(context)?.remarkDao()?.getAllRemarkList()?.forEach { callback(it) }
    }

    fun getRemarkListFromApartment(apartmentID: Long, callback: (remark: List<DaoRemarkEntity>?) -> Unit) {
        callback(AppDatabase.getAppDataBase(context)?.remarkDao()?.getRemarkListFromApartment(apartmentID))
    }

    fun getRemarkListFromApartmentFromRoom(
        apartmentID: Long,
        roomType: String,
        callback: (remark: List<DaoRemarkEntity>?) -> Unit
    ) {
        callback(
            AppDatabase.getAppDataBase(context)?.remarkDao()
                ?.getRemarkListFromApartmentFromRoom(apartmentID, roomType)
        )
    }


    fun setApartmentAsynk(apartment: DaoApartmentEntity) = GlobalScope.launch(Dispatchers.Main) {
        withContext(Dispatchers.IO) {
            AppDatabase.getAppDataBase(context)!!.apartmentDao().insertApartment(apartment)
        }
    }

    fun deleteApartmentAsynk(apartment: DaoApartmentEntity) = GlobalScope.launch(Dispatchers.Main) {
        withContext(Dispatchers.IO) {
            val listIdRemark = mutableListOf<Long>()
            getRemarkListFromApartment(apartment.id!!) {
                it!!.forEach { remark -> deleteRemark(remark) }
            }
            AppDatabase.getAppDataBase(context)?.apartmentDao()?.deleteApartment(apartment)
        }
    }

    fun deleteRemark(remark: DaoRemarkEntity) = GlobalScope.launch(Dispatchers.Main) {
        withContext(Dispatchers.IO) {
            AppDatabase.getAppDataBase(context)?.remarkDao()?.deleteRemark(remark)
        }
    }

    fun setRemarkAsynk(remark: DaoRemarkEntity) = GlobalScope.launch(Dispatchers.Main) {
        withContext(Dispatchers.IO) {
            AppDatabase.getAppDataBase(context)?.remarkDao()?.insertRemark(remark)
        }
    }

    fun getApartmentListAsynk(callback: (apartmentList: List<DaoApartmentEntity>) -> Unit) =
        GlobalScope.launch(Dispatchers.Main) {
            withContext(Dispatchers.IO) {
                callback(AppDatabase.getAppDataBase(context)!!.apartmentDao().getAllApartmentList())
            }
        }


    fun getRemarkListAsynk(callback: (remark: DaoRemarkEntity) -> Unit) = GlobalScope.launch(Dispatchers.Main)
    {
        withContext(Dispatchers.IO) {
            AppDatabase.getAppDataBase(context)?.remarkDao()?.getAllRemarkList()?.forEach { callback(it) }
        }
    }

    fun getRemarkListFromApartmentAsynk(apartmentID: Long, callback: (remark: DaoRemarkEntity) -> Unit) =
        GlobalScope.launch(Dispatchers.Main)
        {
            withContext(Dispatchers.IO) {
                AppDatabase.getAppDataBase(context)?.remarkDao()?.getRemarkListFromApartment(apartmentID)
                    ?.forEach { callback(it) }
            }
        }

    fun getRemarkListFromApartmentFromRoomAsynk(
        apartmentID: Long,
        roomType: String,
        callback: (remark: DaoRemarkEntity) -> Unit
    ) =
        GlobalScope.launch(Dispatchers.Main)
        {
            withContext(Dispatchers.IO) {
                AppDatabase.getAppDataBase(context)?.remarkDao()
                    ?.getRemarkListFromApartmentFromRoom(apartmentID, roomType)
                    ?.forEach { callback(it) }
            }
        }
}