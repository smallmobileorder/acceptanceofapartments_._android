package com.acceptance.apartments.model


data class SubRemark(
    var remark: String = "",
    var levelCritic: Int = 0,
    var actionIfExist: String = ""
)

