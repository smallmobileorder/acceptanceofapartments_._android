package com.acceptance.apartments.model

data class Pairs(
    val title: String?,
    val named: String?
)