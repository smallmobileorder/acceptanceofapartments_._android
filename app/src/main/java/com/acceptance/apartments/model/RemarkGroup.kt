package com.acceptance.apartments.model

data class RemarkGroup(
    var group: String = "",
    var listRemark: MutableList<Remark> = mutableListOf()
)

