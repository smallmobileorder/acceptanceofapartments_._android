package com.acceptance.apartments.model


enum class TypeApartment {
    Apartment,
    Organization,
    Parking
}

enum class TypeReview {
    Clear,
    Preclear,
    Unclear
}
