package com.acceptance.apartments.model

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


@Database(entities = [DaoApartmentEntity::class, DaoRemarkEntity::class], version = 1)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun apartmentDao(): DaoInterfaceApartment
    abstract fun remarkDao(): DaoInterfaceRemark


    companion object {
        var INSTANCE: AppDatabase? = null

        fun getAppDataBase(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    val nameDB = "myDB13"
                    INSTANCE = Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, nameDB).build()
                    clearUnusedData()
                }
            }
            return INSTANCE
        }

        private fun clearUnusedData() {
            GlobalScope.launch(Dispatchers.Main) {
                withContext(Dispatchers.IO) {
                    Repository.getApartmentList {
                        it.forEach { daoApartmentEntity ->
                            if (!daoApartmentEntity.reviewIsComplete) {
                                Repository.deleteApartment(daoApartmentEntity)
                            }
                        }
                    }
                    Repository.getRemarkList {
                        Repository.getApartment(it.apartmentID!!) { apartment ->
                            if (apartment == null) {
                                Repository.deleteRemark(it)
                            }
                        }
                    }
                }
            }
        }
    }
}