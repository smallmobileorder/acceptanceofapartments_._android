package com.acceptance.apartments.model

import androidx.room.*

@Dao
interface DaoInterfaceApartment {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertApartment(apartment: DaoApartmentEntity)

    @Update
    fun updateApartment(apartment: DaoApartmentEntity)

    @Delete
    fun deleteApartment(apartment: DaoApartmentEntity)

    @Query("SELECT * FROM DaoApartmentEntity")
    fun getAllApartmentList(): List<DaoApartmentEntity>

    @Query("SELECT * FROM DaoApartmentEntity WHERE id == :id")
    fun getApartment(id: Long): DaoApartmentEntity?

}