package com.acceptance.apartments.model


data class Remark(
    var remark: String = "",
    var comment: String = "",
    var mapPlaceRemark: MutableMap<String, MutableList<SubRemark>> = mutableMapOf(),
    var currentSubRemark: SubRemark = SubRemark(),
    var docs: MutableList<String> = mutableListOf()
)

