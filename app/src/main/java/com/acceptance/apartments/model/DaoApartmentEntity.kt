package com.acceptance.apartments.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "DaoApartmentEntity")
data class DaoApartmentEntity(
    @PrimaryKey(autoGenerate = true) var id: Long? = null,
    @ColumnInfo(name = "rooms") var rooms: MutableList<String> = mutableListOf(),
    @ColumnInfo(name = "reviews") var reviews: MutableList<String> = mutableListOf(),
    @ColumnInfo(name = "numberContract") var numberContract: String = "",
    @ColumnInfo(name = "dateContract") var dataContract: String = "",
    @ColumnInfo(name = "address") var address: String = "",
    @ColumnInfo(name = "description") var description: String = "",
    @ColumnInfo(name = "numberApartment") var numberRoom: String = "",
    @ColumnInfo(name = "fullName") var fullName: MutableList<String> = mutableListOf(),
    @ColumnInfo(name = "numberPhone") var numberPhone: String = "",
    @ColumnInfo(name = "typeApartment") var typeApartment: TypeApartment = TypeApartment.Apartment,
    @ColumnInfo(name = "typeReview") var typeReview: TypeReview? = null,
    @ColumnInfo(name = "idFirstReview") var idFirstReview: Long? = null,
    @ColumnInfo(name = "progressReview") var progressReview: MutableSet<Remark> = mutableSetOf(),
    @ColumnInfo(name = "progressReviewCount") var progressReviewCount: Int = 0,
    @ColumnInfo(name = "progressReviewAll") var progressReviewAll: MutableMap<String, MutableList<RemarkGroup>> = mutableMapOf(),
    @ColumnInfo(name = "progressIsComplete") var progressIsComplete: Boolean = false,
    @ColumnInfo(name = "reviewIsComplete") var reviewIsComplete: Boolean = true
)
