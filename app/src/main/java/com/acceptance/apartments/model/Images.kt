package com.acceptance.apartments.model

data class Images(
    val pairs: MutableList<Pairs>
)