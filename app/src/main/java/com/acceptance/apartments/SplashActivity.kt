package com.acceptance.apartments

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlin.concurrent.thread


class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intent = Intent(this, MainActivity::class.java)
        thread {
            Thread.sleep(2000)
            runOnUiThread {
                startActivity(intent)
                finish()
            }
        }

    }
}