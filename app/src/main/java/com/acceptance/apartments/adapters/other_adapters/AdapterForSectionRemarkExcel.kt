package com.acceptance.apartments.adapters.other_adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import com.acceptance.apartments.R
import com.acceptance.apartments.adapters.AbstractAdapter
import com.acceptance.apartments.model.DaoRemarkEntity
import com.acceptance.apartments.view_holders.AbstractViewHolder
import com.acceptance.apartments.view_holders.other_view_holder.ListRemarkSectionExcelViewHolder

class AdapterForSectionRemarkExcel(override var list: List<Any>, val listRemark: List<DaoRemarkEntity>) :
    AbstractAdapter(list) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_remark_point_excel, parent, false)
        holder = ListRemarkSectionExcelViewHolder(view)
        return holder
    }

    override fun onBindViewHolder(holder: AbstractViewHolder, position: Int) {
        holder.bind(listRemark[position])
    }

}