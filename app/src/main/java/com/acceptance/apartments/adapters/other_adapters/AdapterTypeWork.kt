package com.acceptance.apartments.adapters.other_adapters

import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.acceptance.apartments.R
import com.acceptance.apartments.model.RemarkGroup
import com.acceptance.apartments.view_holders.other_view_holder.TypeWorkViewHolder


class AdapterTypeWork(
    var list: List<String>,
    containerView: ConstraintLayout,
    var isEnabledList: List<Boolean>,
    var isCheckedList: List<Boolean>,
    listBackground: List<String>,
    listIcon: List<String>,
    listItem: MutableMap<String, MutableList<RemarkGroup>>,
    listIconInvisible: List<String>
) {

    private val listHolder = mutableListOf<TypeWorkViewHolder>()

    init {
        for (position in list.size - 1 downTo 0) {
            val view = LayoutInflater.from(containerView.context).inflate(R.layout.item_type_work, containerView, false)
            val params = view.layoutParams as ConstraintLayout.LayoutParams
            val height = containerView.display.height
            val myHeight = 2100
            params.height =
                if (position == 0) (300.0 * height / myHeight).toInt() else (530.0 * height / myHeight).toInt()
            params.topMargin = (230.0 * height / myHeight).toInt() * (position - 1)
            view.layoutParams = params
            val holder = TypeWorkViewHolder(view)
            holder.bind(
                list[position],
                isEnabledList[position],
                isCheckedList[position],
                listBackground[position],
                listIcon[position],
                listItem[listItem.keys.toList()[position]]!!,
                listIconInvisible[position]
            )
            listHolder.add(holder)
            containerView.addView(view)
        }
    }

    fun notifyDataChanged() {
        listHolder.forEach { it.notifyItemChanged() }
    }

}