package com.acceptance.apartments.adapters.other_adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.CheckBox
import androidx.core.content.res.ResourcesCompat
import com.acceptance.apartments.R

class GridAdapter(private val size: Int, private val namedList: List<String?>? = null) : BaseAdapter() {

    private var listItem = mutableListOf<Boolean>()

    private var listView = mutableSetOf<CheckBox>()

    var checkBox: CheckBox? = null

    init {
        for (i in 0 until size)
            listItem.add(i == 0)
    }

    override fun getItem(position: Int) = listItem[position]

    override fun getItemId(position: Int) = position.toLong()

    override fun getCount() = size

    @SuppressLint("ViewHolder", "SetTextI18n")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = LayoutInflater.from(parent!!.context).inflate(R.layout.item_grid, parent, false) as CheckBox

        view.apply {
            setOnClickListener {
                this@GridAdapter.setIsSelected(this)
            }
            view.text = if (namedList.isNullOrEmpty() || namedList[position] == null) {
                "" + (position + 1)
            } else namedList[position]
            isChecked = listItem[position]
        }
        val font = ResourcesCompat.getFont(view.context, R.font.century_gothic)
        view.typeface = font

        listView.add(view)
        if (position == 0 && checkBox == null) {
            checkBox = view
        }
        return view
    }

    fun setIsSelected(position: CheckBox) {

        listView.forEach {
            if (it.text.toString() != position.text) {
                it.isChecked = false
            } else {
                it.isChecked = true
                checkBox = it
            }
        }
    }
}