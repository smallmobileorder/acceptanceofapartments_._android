package com.acceptance.apartments.adapters.other_adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import com.acceptance.apartments.R
import com.acceptance.apartments.adapters.AbstractAdapter
import com.acceptance.apartments.view_holders.AbstractViewHolder
import com.acceptance.apartments.view_holders.other_view_holder.HelpViewHolder

class AdapterForHelp(
    override var list: List<Any>,
    private val idList: Int
) :
    AbstractAdapter(list) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_help_section, parent, false)
        holder = HelpViewHolder(view, idList)
        return holder
    }

}