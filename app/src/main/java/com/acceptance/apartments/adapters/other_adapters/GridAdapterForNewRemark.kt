package com.acceptance.apartments.adapters.other_adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import com.acceptance.apartments.R
import com.acceptance.apartments.adapters.AbstractAdapter
import com.acceptance.apartments.model.SubRemark
import com.acceptance.apartments.view_holders.AbstractViewHolder
import com.acceptance.apartments.view_holders.other_view_holder.PointNewRemarkViewHolder
import kotlinx.android.synthetic.main.item_new_remark_grid_point.view.*

class GridAdapterForNewRemark(private val namedList: List<SubRemark>) : AbstractAdapter(namedList) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_new_remark_grid_point, parent, false)
        holder = PointNewRemarkViewHolder(view)
        return holder
    }

    override fun onBindViewHolder(holder: AbstractViewHolder, position: Int) {
        holder.bind(namedList[position], this)
    }

    var listView = mutableSetOf<PointNewRemarkViewHolder>()

    var pointNewRemarkViewHolder: PointNewRemarkViewHolder? = null

    fun setIsSelected(name: String) {

        listView.forEach {
            if (name != it.view.crp_description_text_view.text) {
                it.view.crp_is_valid_check_box.isChecked = false
            } else {
                it.view.crp_is_valid_check_box.isChecked = true
                pointNewRemarkViewHolder = it
            }
        }
    }
}