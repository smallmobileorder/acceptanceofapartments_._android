package com.acceptance.apartments.adapters.other_adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import com.acceptance.apartments.R
import com.acceptance.apartments.adapters.AbstractAdapter
import com.acceptance.apartments.model.DaoRemarkEntity
import com.acceptance.apartments.model.RemarkGroup
import com.acceptance.apartments.view_holders.AbstractViewHolder
import com.acceptance.apartments.view_holders.other_view_holder.ListRemarkSectionViewHolder
import com.acceptance.apartments.view_holders.other_view_holder.ListRemarkViewHolder

class AdapterForListRemark(
    override var list: List<Any>,
    private val remarkGroup: MutableList<RemarkGroup>,
    val type: String,
    var listDao: MutableList<DaoRemarkEntity>
) :
    AbstractAdapter(list) {

    val mutableListItem = mutableListOf<ListRemarkSectionViewHolder>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_remark_section, parent, false)
        return ListRemarkViewHolder(view, listDao, this)
    }

    override fun onBindViewHolder(holder: AbstractViewHolder, position: Int) {
        holder.bind(remarkGroup[position], type)
    }

    fun notifyCheck() {
        mutableListItem.forEach { it.notifyCheck() }
    }

}