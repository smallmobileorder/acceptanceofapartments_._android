package com.acceptance.apartments.adapters.other_adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import com.acceptance.apartments.R
import com.acceptance.apartments.adapters.AbstractAdapter
import com.acceptance.apartments.model.DaoApartmentEntity
import com.acceptance.apartments.view_holders.AbstractViewHolder
import com.acceptance.apartments.view_holders.other_view_holder.TypeReviewViewHolder


class AdapterTypeReview(
    override var list: List<Any>,
    var listApartment: MutableList<DaoApartmentEntity>,
    private val isResult: Boolean = false,
    val isMain: Boolean = false

) : AbstractAdapter(list) {

    override fun getItemCount(): Int {
        return listApartment.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_room_for_review, parent, false)
        holder = TypeReviewViewHolder(view, isResult)
        return holder
    }

    override fun onBindViewHolder(holder: AbstractViewHolder, position: Int) {
        holder.bind(listApartment[position])
    }

    fun removeAt(position: Int, callback: (apartment: DaoApartmentEntity) -> Unit) {
        notifyItemRemoved(position)
        callback(listApartment[position])
        listApartment.removeAt(position)
    }

    fun insertAt(position: Int, apartment: DaoApartmentEntity) {
        listApartment.add(position, apartment)
        notifyItemInserted(position)
    }


}