package com.acceptance.apartments.adapters.other_adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import com.acceptance.apartments.R
import com.acceptance.apartments.adapters.AbstractAdapter
import com.acceptance.apartments.view_holders.AbstractViewHolder
import com.acceptance.apartments.view_holders.other_view_holder.ProgressResultViewHolder

class AdapterForProgressItemInResult(
    override var list: List<Any>,
    private val remarks: Map<String, Int>,
    private val max: Int
) :
    AbstractAdapter(list) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_progress_result_review, parent, false)
        holder = ProgressResultViewHolder(view)
        return holder
    }

    override fun onBindViewHolder(holder: AbstractViewHolder, position: Int) {
        holder.bind(remarks.entries.toList()[position], max)
    }
}