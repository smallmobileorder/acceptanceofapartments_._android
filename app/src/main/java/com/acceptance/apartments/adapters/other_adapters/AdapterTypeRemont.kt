package com.acceptance.apartments.adapters.other_adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import com.acceptance.apartments.R
import com.acceptance.apartments.adapters.AbstractAdapter
import com.acceptance.apartments.model.RemarkGroup
import com.acceptance.apartments.view_holders.AbstractViewHolder
import com.acceptance.apartments.view_holders.other_view_holder.TypeRemontViewHolder


class AdapterTypeRemont(
    override var list: List<Any>,
    var map: MutableMap<String, MutableList<RemarkGroup>>
) : AbstractAdapter(list) {

    companion object {
        val arrayClear = arrayListOf(
            "Входная дверь",
            "Отделка (Чистовая)",
            "Окна (Чистовая)",
            "Фасад",
            "Витражи",
            "Сантехника (Чистовая)",
            "Электрика",
            "Парковочное место"
        )
        val arrayPreClear = arrayListOf(
            "Входная дверь",
            "Отделка (Предчистовая)",
            "Окна (Черновая или Предчистовая)",
            "Фасад",
            "Витражи",
            "Сантехника (Черновая или Предчистовая)",
            "Электрика",
            "Парковочное место"
        )
        val arrayUnClear = arrayListOf(
            "Входная дверь",
            "Отделка (Черновая)",
            "Окна (Черновая или Предчистовая)",
            "Фасад",
            "Витражи",
            "Сантехника (Черновая или Предчистовая)",
            "Электрика",
            "Парковочное место"
        )
    }

    val currentList = mutableListOf(arrayClear, arrayPreClear, arrayUnClear, arrayListOf())

    init {
        if (list.size == 3) {
            currentList.remove(arrayClear)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_type_remont, parent, false)
        holder = TypeRemontViewHolder(view)
        return holder
    }

    override fun onBindViewHolder(holder: AbstractViewHolder, position: Int) {
        holder.bind(list[position], currentList[position], map)

    }
}