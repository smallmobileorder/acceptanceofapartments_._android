package com.acceptance.apartments.adapters.other_adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import com.acceptance.apartments.R
import com.acceptance.apartments.adapters.AbstractAdapter
import com.acceptance.apartments.model.DaoRemarkEntity
import com.acceptance.apartments.model.RemarkGroup
import com.acceptance.apartments.view_holders.AbstractViewHolder
import com.acceptance.apartments.view_holders.other_view_holder.ListRemarkSectionViewHolder

class AdapterForSectionRemark(
    override var list: List<Any>,
    private val group: RemarkGroup,
    val type: String,
    var daoList: MutableList<DaoRemarkEntity>,
    var adapterForListRemark: AdapterForListRemark
) :
    AbstractAdapter(list) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_remark_point, parent, false)
        holder = ListRemarkSectionViewHolder(view, adapterForListRemark)
        return holder
    }

    override fun onBindViewHolder(holder: AbstractViewHolder, position: Int) {
        holder.bind(group.listRemark[position], type, daoList)
    }
}