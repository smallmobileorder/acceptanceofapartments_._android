package com.acceptance.apartments.adapters.other_adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import com.acceptance.apartments.R
import com.acceptance.apartments.adapters.AbstractAdapter
import com.acceptance.apartments.model.SubRemark
import com.acceptance.apartments.view_holders.AbstractViewHolder
import com.acceptance.apartments.view_holders.other_view_holder.ListNewRemarkViewHolder

class AdapterForNewRemark(override var list: List<Any>, private val map: MutableMap<String, List<SubRemark>>) :
    AbstractAdapter(list) {
    private val listRemark = mutableListOf<ListNewRemarkViewHolder>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_new_remark_section, parent, false)
        holder = ListNewRemarkViewHolder(view)
        listRemark.add(holder as ListNewRemarkViewHolder)
        return holder
    }

    override fun onBindViewHolder(holder: AbstractViewHolder, position: Int) {
        holder.bind(map.keys.toList()[position], map.values.toList()[position])
    }

    fun getListRemarks(): List<SubRemark?> {
        val resultList = mutableListOf<SubRemark?>()
        listRemark.forEach {
            if (it.getCurrentSubRemark() != null) {
                val subRemark = it.getCurrentSubRemark()!!.subRemark
                resultList.add(subRemark)
            } else {
                resultList.add(null)
            }
        }
        return resultList
    }

    fun getCurrentSubRemark(): SubRemark? {
        var result: SubRemark? = null

        map["Возможные варианты"]?.forEach { if (getListRemarks().contains(it)) result = it }
        if (result == null) map["Место"]?.forEach { if (getListRemarks().contains(it)) result = it }
        if (result == null) map["Помещение"]?.forEach { if (getListRemarks().contains(it)) result = it }
        if (result == null) map["Часть оконного блока"]?.forEach { if (getListRemarks().contains(it)) result = it }
        return result
    }
}