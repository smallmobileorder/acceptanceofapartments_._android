package com.acceptance.apartments.adapters.other_adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import com.acceptance.apartments.R
import com.acceptance.apartments.adapters.AbstractAdapter
import com.acceptance.apartments.fragments.other_fragments.ListReprimandFragment
import com.acceptance.apartments.model.DaoRemarkEntity
import com.acceptance.apartments.view_holders.AbstractViewHolder
import com.acceptance.apartments.view_holders.other_view_holder.ListReprimandViewHolder


class AdapterListReprimand(
    override var list: List<Any>,
    var listRemark: List<DaoRemarkEntity>,
    val listReprimandFragment: ListReprimandFragment,
    val isMainList: Boolean = false
) : AbstractAdapter(list) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_reprimand, parent, false)
        holder = ListReprimandViewHolder(view, isMainList)
        return holder
    }

    override fun onBindViewHolder(holder: AbstractViewHolder, position: Int) {
        holder.bind(listRemark[position], listReprimandFragment)
    }
}