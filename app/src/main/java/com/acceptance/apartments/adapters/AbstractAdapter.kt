package com.acceptance.apartments.adapters

import androidx.recyclerview.widget.RecyclerView
import com.acceptance.apartments.view_holders.AbstractViewHolder

abstract class AbstractAdapter(open var list: List<Any>) : RecyclerView.Adapter<AbstractViewHolder>() {
    lateinit var holder: AbstractViewHolder

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: AbstractViewHolder, position: Int) {
        holder.bind(list[position])
    }
}