package com.acceptance.apartments.utils.common

import android.annotation.SuppressLint
import android.content.res.ColorStateList
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("SimpleDateFormat")
fun Long.toDate() = SimpleDateFormat("dd.MM.yyyy").format(Date(this))!!

fun Int.setAlpha(percent: Float): Int {
    val R = this % 256
    val G = (this / 256) % 256
    val B = (this / 256 / 256) % 256
    return R + G * 256 + B * 256 * 256 + (percent * 255).toInt() * 256 * 256 * 256
}

fun Int.toColorStateList() = ColorStateList(arrayOf(intArrayOf()), intArrayOf(this))