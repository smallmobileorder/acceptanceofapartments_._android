package com.acceptance.apartments.utils.common

enum class TypeComplete(val value: Boolean?) {
    COMPLETE(true),
    NOT_COMPLETE(false),
    NOT_FOUND(null)
}