package com.acceptance.apartments.fragments.other_fragments

import android.os.Bundle
import android.view.View
import com.acceptance.apartments.R
import com.acceptance.apartments.adapters.other_adapters.AdapterMenu
import com.acceptance.apartments.fragments.AbstractFragment
import kotlinx.android.synthetic.main.component_recycler_view.*


open class MenuFragment : AbstractFragment(R.layout.fragment_menu) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainRecycler?.adapter = AdapterMenu(resources.getStringArray(R.array.menu).toList(), R.array.menu)
        activity.clearFragments()
    }
}