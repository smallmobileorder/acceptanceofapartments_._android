package com.acceptance.apartments.fragments.other_fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context.LAYOUT_INFLATER_SERVICE
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.graphics.RectF
import android.media.ExifInterface
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.text.TextUtils
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.acceptance.apartments.MainActivity
import com.acceptance.apartments.R
import com.acceptance.apartments.adapters.other_adapters.AdapterForNewRemark
import com.acceptance.apartments.fragments.AbstractFragment
import com.acceptance.apartments.model.*
import com.acceptance.apartments.utils.common.setAlpha
import com.acceptance.apartments.utils.common.toColorStateList
import kotlinx.android.synthetic.main.component_header.*
import kotlinx.android.synthetic.main.component_pop_up_photo.view.*
import kotlinx.android.synthetic.main.component_recycler_view.*
import kotlinx.android.synthetic.main.fragment_new_remark.*
import kotlinx.android.synthetic.main.fragment_new_remark.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.intellij.lang.annotations.MagicConstant
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class NewRemarkFragment(private val mapItem: Remark? = null, val type: String = "Новое") :
    AbstractFragment(R.layout.fragment_new_remark) {

    private var photoBitmap: String = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var color = activity.model.backGroundFragment.second
        if (mapItem == null) {
            remarkText.visibility = VISIBLE
            remarkCard.visibility = VISIBLE
            remarkEditText.visibility = VISIBLE
        } else {
            remarkText.visibility = GONE
            remarkCard.visibility = GONE
            remarkEditText.visibility = GONE
        }
        view.save_btn.setOnClickListener {
            if (checkOnLatent()) {
                updateRemark()
            } else {
                Toast.makeText(activity, "Не все поля заполнены", Toast.LENGTH_SHORT).show()
            }
        }

        view.remarkText.setTextColor(color)
        view.commentsText.setTextColor(color)
        activity.headerLine.setBackgroundColor(color)
        if (type == "Витражи") {
            color = view.resources.getColor(R.color.colorVitrag)
        }
        view.save_btn.backgroundTintList = color.toColorStateList()
        view.add_photo_btn.iconTint = color.toColorStateList()
        view.add_photo_btn.strokeColor = color.toColorStateList()
        view.add_photo_btn.setTextColor(color.toColorStateList())
        view.commentsEditText.setTextColor(color)
        view.commentsCard.strokeColor = color
        view.remarkCard.strokeColor = color
        view.commentsEditText.setHintTextColor(color.setAlpha(0.4f))
        view.remarkEditText.setHintTextColor(color.setAlpha(0.4f))
        view.remarkEditText.setTextColor(color)

        view.add_photo_btn.setOnClickListener {
            if (photoBitmap.isEmpty()) takePhoto()
            else {
                val activity = (view.context as MainActivity)
                val inflater = activity.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater?
                val popupView = inflater!!.inflate(R.layout.component_pop_up_photo, null)
                val width = LinearLayout.LayoutParams.MATCH_PARENT
                val height = LinearLayout.LayoutParams.MATCH_PARENT
                val options = BitmapFactory.Options()
                options.inPreferredConfig = Bitmap.Config.ARGB_8888
                val bitmap = BitmapFactory.decodeFile(currentPhotoPath, options)
                popupView.photoReprimand.setImageBitmap(bitmap)
                val focusable = true
                val popupWindow = PopupWindow(popupView, width, height, focusable)
                popupView.setOnClickListener {
                    popupWindow.dismiss()
                }
                popupWindow.elevation = 20f
                popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0)
            }
        }
        val map = mapItem?.mapPlaceRemark ?: mutableMapOf()
        if ((!map.contains("Помещение") || map["Помещение"]!!.isEmpty()) && (activity.model.newApartmentEntity.typeApartment != TypeApartment.Parking)) {
            val result = mutableListOf<SubRemark>()
            activity.model.newApartmentEntity.rooms.forEach { result.add(SubRemark(it)) }
            if (activity.model.newApartmentEntity.typeApartment == TypeApartment.Apartment) when (type) {
                "Входная дверь" -> {
                    result.clear()
                }
                "Фасад" -> {
                    result.clear()
                }
                "Витражи" -> {
                    result.clear()
                }
            }
            map["Выберите помещение"] = result
        }

        val newMap = mutableMapOf<String, List<SubRemark>>()
        map.forEach { value -> if (value.value.isNotEmpty()) newMap[value.key] = value.value }
        mainRecycler?.adapter = AdapterForNewRemark(
            newMap.keys.toList(),
            newMap
        )
    }

    private fun checkOnLatent(): Boolean {
        var flag = false
        (mainRecycler?.adapter as AdapterForNewRemark?)?.getListRemarks()?.forEach {
            if (it == null) {
                flag = true
                return@forEach
            }
        }
        if (mapItem == null && remarkEditText.text.isEmpty()) {
            flag = true
        }
        return !flag
    }

    /**
     * Makes a photo from camera.
     */

    private fun takePhoto() {
        val cameraPermissionResult = ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA)
        if (cameraPermissionResult != PackageManager.PERMISSION_GRANTED) {
            val permissions = arrayOf(Manifest.permission.CAMERA)
            ActivityCompat.requestPermissions(activity, permissions, CAMERA_PERMISSION_CODE)
        } else {
            val photoIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            photoIntent.putExtra("android.intent.extras.LENS_FACING_FRONT", 1)
            photoIntent.putExtra("android.intent.extra.USE_FRONT_CAMERA", true)
            val builder = StrictMode.VmPolicy.Builder()
            StrictMode.setVmPolicy(builder.build())
            photoIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(createImageFile()))
            println(photoIntent.extras)
            activity.startActivityForResult(photoIntent, CAMERA_REQUEST_CODE)
        }
    }

    /**
     * Saves remark clarification.
     */

    private fun updateRemark() {
        //Update photo:
        activity.model.newRemark = DaoRemarkEntity()
        val remark = activity.model.newRemark
        remark.comment = mapItem?.comment ?: ""
        remark.comment += "   "
        remark.apartmentID = activity.model.newApartmentEntity.id
        val remarksAndRoom = (mainRecycler?.adapter as AdapterForNewRemark?)?.getListRemarks()
        if (activity.model.newApartmentEntity.typeApartment == TypeApartment.Parking) {
            remark.typeRoom = getString(R.string.parking_item)
        }
        if (activity.model.newApartmentEntity.typeApartment == TypeApartment.Apartment) when (type) {
            "Входная дверь" -> remark.typeRoom = getString(R.string.koridor)
            "Фасад" -> remark.typeRoom = getString(R.string.balkon)
            "Витражи" -> remark.typeRoom = getString(R.string.balkon)
        }
        if (!remarksAndRoom.isNullOrEmpty() && remarksAndRoom.isNotEmpty()) {
            val remarks: List<SubRemark?>
            if (remark.typeRoom.isEmpty()) {
                remark.typeRoom = remarksAndRoom.last()!!.remark
                remarks = remarksAndRoom.subList(0, remarksAndRoom.size - 1)
            } else {
                remarks = remarksAndRoom
            }
            remarks.forEach {
                remark.comment += "(" + it!!.remark + ") "
            }
        }
        if (commentsEditText.text.toString().trim().isNotEmpty()) remark.comment += "(" + commentsEditText.text.toString().trim() + ") "
        mapItem?.docs?.forEach { if (it.isNotEmpty()) remark.docs += "($it) " }
        if (remarkEditText.visibility == VISIBLE) {
            remark.nameRemark = remarkEditText.text.toString()
            remark.actionIfExist = remark.comment
        } else {
            val currentSubRemark = (mainRecycler?.adapter as AdapterForNewRemark?)?.getCurrentSubRemark()
            if (currentSubRemark != null) mapItem!!.currentSubRemark = currentSubRemark
            remark.levelCritic = mapItem!!.currentSubRemark.levelCritic
            remark.actionIfExist = mapItem.currentSubRemark.actionIfExist
            remark.nameRemark = mapItem.remark
        }
        remark.photo = this.photoBitmap
        remark.typeRemark = type
        loadData(remark)
    }

    private fun loadData(remarkEntity: DaoRemarkEntity) = GlobalScope.launch(Dispatchers.Main) {
        withContext(Dispatchers.IO) {
            Repository.setRemark(remarkEntity)
            println(" remark = " + remarkEntity.apartmentID)
        }
        withContext(Dispatchers.Main) {
            Toast.makeText(context, "Данные сохранены", Toast.LENGTH_SHORT).show()
            activity.onBackPressed()
        }
    }


    private fun saveBitmapToFile(file: File) {
        try {

            // BitmapFactory options to downsize the image
            var options = BitmapFactory.Options()
            options.inJustDecodeBounds = true
            options.inSampleSize = 6
            // factor of downsizing the image

            var inputStream = FileInputStream(file)
            inputStream.close()

            val originalWidth = options.outWidth
            val originalHeight = options.outHeight

            if (originalWidth > 0) {

                val reqWidth = 720
                val reqHeight = (reqWidth * originalHeight) / originalWidth
                Log.d(
                    "new image ", "getDropboxIMGSize: " + reqHeight + "    " +
                            reqWidth
                )
                // decode full image pre-resized
                inputStream = FileInputStream(file)
                options = BitmapFactory.Options()

                // calc rought re-size (this is no exact resize)
                options.inSampleSize = Math.max(
                    originalWidth / reqWidth, originalHeight
                            / reqHeight
                )
                // decode full image
                val roughBitmap = BitmapFactory.decodeStream(inputStream, null, options)!!

                // calc exact destination size
                val m = Matrix()
                val inRect = RectF(
                    0f, 0f, roughBitmap.width.toFloat(),
                    roughBitmap.height.toFloat()
                )
                val outRect = RectF(0f, 0f, reqWidth.toFloat(), reqHeight.toFloat())
                m.setRectToRect(inRect, outRect, Matrix.ScaleToFit.CENTER)
                val values = FloatArray(9)
                m.getValues(values)

                // resize bitmap
                val resizedBitmap = Bitmap.createScaledBitmap(
                    roughBitmap,
                    ((roughBitmap.width * values[0]).toInt()),
                    ((roughBitmap.height * values[4]).toInt()), true
                )

                // override resized bitmap image
                file.createNewFile()
                val out = FileOutputStream(file)
                resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 70, out)
            }

        } catch (e: IOException) {
            Log.e("Image", e.message, e)
        }
    }

    private fun getOrientationDegree(imagePath: String): Int {
        if (TextUtils.isEmpty(imagePath)) {
            return 0
        }
        try {
            val exifInterface = ExifInterface(imagePath)
            return when (exifInterface.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_NORMAL
            )) {
                ExifInterface.ORIENTATION_ROTATE_90 -> 90
                ExifInterface.ORIENTATION_ROTATE_180 -> 180
                ExifInterface.ORIENTATION_ROTATE_270 -> 270
                else -> 0
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return 0
    }

    private fun resetOrientation(filePath: String) {
        if (TextUtils.isEmpty(filePath)) {
            return
        }
        var bitmap = BitmapFactory.decodeFile(filePath)//
        val degree = getOrientationDegree(filePath)
        bitmap = rotateBitmap(bitmap, degree)
        val out = FileOutputStream(filePath)
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out)
    }

    private fun rotateBitmap(bitmap: Bitmap?, degree: Int): Bitmap? {
        if (degree == 0 || bitmap == null) {
            return bitmap
        }
        val matrix = Matrix()
        matrix.postRotate(degree.toFloat())
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
    }

    private fun displayError() {
        val context = context
        val toast = Toast.makeText(context, ERROR_MESSAGE, Toast.LENGTH_LONG)
        toast.show()
    }

    /**
     * Returns activity results.
     */

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == CAMERA_PERMISSION_CODE && grantResults.isNotEmpty()) {
            val cameraPermissionResult = grantResults[0]
            if (cameraPermissionResult == PackageManager.PERMISSION_GRANTED) {
                this.takePhoto()
            } else {
                this.displayError()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == CAMERA_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            this.photoBitmap = currentPhotoPath
            val storageDir = File(currentPhotoPath)
            saveBitmapToFile(storageDir)
            resetOrientation(currentPhotoPath)
            view!!.add_photo_btn.icon = resources.getDrawable(R.drawable.photo_exist)
            view!!.add_photo_btn.text = "Фото добавлено"
        }
    }

    private lateinit var currentPhotoPath: String

    @SuppressLint("SimpleDateFormat")
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
        return createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = absolutePath
        }
    }


    companion object {

        @MagicConstant
        private const val CAMERA_REQUEST_CODE = 777

        @MagicConstant
        private const val CAMERA_PERMISSION_CODE = 10012

        private const val ERROR_MESSAGE = "Камера недоступна"
    }
}