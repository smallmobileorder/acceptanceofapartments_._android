package com.acceptance.apartments.fragments.other_fragments

import com.acceptance.apartments.R
import com.acceptance.apartments.model.Repository
import kotlinx.android.synthetic.main.component_botton_of_header.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TypeReviewSecondFragment : TypeReviewFragment() {
    override val isResult: Boolean = true

    override fun loadData() = GlobalScope.launch(Dispatchers.Main) {
        withContext(Dispatchers.IO) {
            Repository.getApartmentList {
                apartmentList = it.filter { daoApartmentEntity -> daoApartmentEntity.reviewIsComplete }.toMutableList()
            }
        }
        withContext(Dispatchers.Main) {
            updateRecycler()
        }
    }

    override fun initBottomView() {
        bottomOfHeader.text = getString(R.string.type_review_second_fragment_text)
    }
}