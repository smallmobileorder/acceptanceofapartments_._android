package com.acceptance.apartments.fragments.other_fragments

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.acceptance.apartments.MainActivity.Companion.fromJsonSection
import com.acceptance.apartments.R
import com.acceptance.apartments.adapters.other_adapters.AdapterForHelp
import com.acceptance.apartments.fragments.AbstractFragment
import com.acceptance.apartments.model.SectionHelp
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.component_btn_back.*
import kotlinx.android.synthetic.main.component_header.view.*
import kotlinx.android.synthetic.main.component_recycler_view.*
import kotlinx.android.synthetic.main.fragment_help.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HelpFragment(var idRaw: Int, private val index: Int = 0) :
    AbstractFragment(R.layout.fragment_help) {

    var listSection = mutableListOf<SectionHelp>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initBottomView()
        initButtonBack()
    }


    private fun loadData() = GlobalScope.launch(Dispatchers.Main) {
        withContext(Dispatchers.IO) {
            listSection = fromJsonSection(resources.openRawResource(idRaw).bufferedReader())
        }
        withContext(Dispatchers.Main)
        {
            mainRecycler?.adapter = AdapterForHelp(listSection, idRaw)
            (mainRecycler?.layoutManager as LinearLayoutManager).scrollToPosition(index)
        }
    }

    override fun onResume() {
        super.onResume()
        updateIcon()
        loadData()
    }

    private fun updateIcon() {
        when (idRaw) {
            R.raw.help -> {
                titleText.text = "Помощь"
                activity.head.iconLayout.setImageDrawable(resources.getDrawable(R.drawable.question_mark))
            }
            else -> {
                activity.head.iconLayout.setImageDrawable(resources.getDrawable(R.drawable.literature_filled))
                titleText.text = when (idRaw) {
                    R.raw.what_you_need_on_review -> "Что взять на осмотр"
                    R.raw.process_review -> "Процесс приемки"
                    R.raw.critical_remark -> "Критичность замечаний"
                    R.raw.garanti_time -> "Гарантийный срок"
                    R.raw.glossary -> "Глоссарий"
                    else -> "Общие рекомендации"
                }
            }
        }
    }

    private fun initButtonBack() {
        btnBack.setOnClickListener { activity.onBackPressed() }
    }


    private fun initBottomView() {
        titleText.text = getString(R.string.choose_room)
    }
}