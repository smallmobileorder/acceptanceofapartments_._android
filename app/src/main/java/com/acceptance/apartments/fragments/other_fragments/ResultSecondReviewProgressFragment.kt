package com.acceptance.apartments.fragments.other_fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.acceptance.apartments.R
import com.acceptance.apartments.model.DaoApartmentEntity
import com.acceptance.apartments.model.Repository
import com.acceptance.apartments.model.TypeApartment
import kotlinx.android.synthetic.main.fragment_result_second_review_progress.*
import kotlinx.android.synthetic.main.fragment_result_second_review_progress.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ResultSecondReviewProgressFragment(private val apartmentEntity: DaoApartmentEntity) :
    ListResultReviewFragment(apartmentEntity, R.layout.fragment_result_second_review_progress) {
    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.titleText.text = "Повторный осмотр от " + apartmentEntity.reviews.last()
        view.btnExport.setOnClickListener {
            clickListener()
        }
        view.btnComplete.setOnClickListener {
            activity.initFragment(MenuFragment())
        }
    }

    override fun updateList(room: String) = GlobalScope.launch(Dispatchers.Main) {
        withContext(Dispatchers.IO) {
            Repository.getRemarkListFromApartmentFromRoom(apartmentEntity.id!!, room) {
                val resList = it!!.filter { remark -> remark.isShow && remark.isComplete != true }
                if (resList.isNotEmpty()) {
                    map[room] = resList
                }
            }
        }
        withContext(Dispatchers.Main) {
            checkCompleteRemark()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun checkCompleteRemark() {
        var notComplete = 0
        var allRemarkCount = 0
        map.forEach { entry -> notComplete += entry.value.size }
        fullMap.forEach { entry -> allRemarkCount += entry.value.size }
        val resultCount = if (allRemarkCount < 1)
            100 else (100 - notComplete * 100.0 / allRemarkCount).toInt()
        countSuccess?.text = resultCount.toString()
        countSuccess?.setTextColor(
            ContextCompat.getColor(
                activity, when (resultCount) {
                    in 90..100 -> R.color.color100_84
                    in 80..89 -> R.color.color83_67
                    in 70..79 -> R.color.color66_50
                    else -> R.color.color49__
                }
            )
        )
        textBuilder?.text =
            "Замечания по " + when (apartmentEntity.typeApartment) {
                TypeApartment.Apartment -> "квартире"
                TypeApartment.Organization -> "нежилому помещению"
                else -> "парковочному месту"
            }
        if (apartmentEntity.numberRoom.isNotEmpty())
            textBuilder?.text = textBuilder?.text.toString() + " №" + apartmentEntity.numberRoom
        textBuilder?.text =
            textBuilder?.text.toString() + ", выявленные на осмотре от " + apartmentEntity.reviews.first() + " устранены"
        if (resultCount != 100) {
            remarkNotComplete?.visibility = View.VISIBLE
            textBuilder?.text = textBuilder?.text.toString() + " частично"
        }
    }
}