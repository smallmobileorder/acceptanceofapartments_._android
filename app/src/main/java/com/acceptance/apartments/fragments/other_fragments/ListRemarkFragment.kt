package com.acceptance.apartments.fragments.other_fragments

import android.os.Bundle
import android.view.View
import androidx.core.content.res.ResourcesCompat
import com.acceptance.apartments.MainActivity
import com.acceptance.apartments.R
import com.acceptance.apartments.adapters.other_adapters.AdapterForListRemark
import com.acceptance.apartments.fragments.AbstractFragment
import com.acceptance.apartments.model.*
import com.acceptance.apartments.utils.common.setAlpha
import com.acceptance.apartments.utils.common.toColorStateList
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.component_header.*
import kotlinx.android.synthetic.main.component_recycler_view.*
import kotlinx.android.synthetic.main.fragment_list_remark.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class ListRemarkFragment(val map: MutableList<RemarkGroup> = mutableListOf(), private val type: String = "") :
    AbstractFragment(R.layout.fragment_list_remark) {

    var result = listOf<DaoRemarkEntity>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var color = activity.model.backGroundFragment.second
        val alphaColor = color.setAlpha(0.4f)
        activity.main_background.setBackgroundColor(alphaColor)
        view.card_add_new.setCardBackgroundColor(color)
        activity.checkAllBtn.setTextColor(color)
        activity.checkAllBtn.buttonTintList = color.toColorStateList()
        view.crp_add_clarification_button.iconTint = alphaColor.toColorStateList()
        view.crp_add_clarification_button.setTextColor(alphaColor)
        view.save_btn.backgroundTintList = color.toColorStateList()
        activity.headerLine.setBackgroundColor(color)

        if (color == view.resources.getColor(R.color.colorVitragFon)) {
            color = view.resources.getColor(R.color.colorVitrag)
        }
        view.crp_description_text_view.setTextColor(color)

        val listener = View.OnClickListener {
            activity.model.isCustomRemark = true
            activity.initFragment(NewRemarkFragment(type = type))
        }
        view.crp_add_clarification_button.setOnClickListener(listener)
        view.crp_description_text_view.setOnClickListener(listener)
        view.save_btn.setOnClickListener {
            if (activity.model.newApartmentEntity.typeApartment == TypeApartment.Parking) {
                val fullList = mutableListOf<Remark>()
                map.forEach { entry -> fullList.addAll(entry.listRemark) }
                if (activity.model.newApartmentEntity.progressReview.containsAll(fullList))
                    (view.context as MainActivity).initFragment(ResultReviewProgressFragment(
                        activity.model.newApartmentEntity,
                        true
                    ))
            } else activity.onBackPressed()
        }

        activity.iconLayout.setImageDrawable(activity.model.backGroundFragment.first)
        activity.checkAllBtn.setOnCheckedChangeListener { _, isChecked ->
            if (!isChecked) {
                activity.checkAllBtn.text = getString(R.string.checkAll)
            } else {
                activity.checkAllBtn.text = getString(R.string.uncheckAll)
            }
        }
        var existCheck = true
        map.forEach { entry ->
            entry.listRemark.forEach { remark ->
                if (activity.model.newApartmentEntity.progressReview.find { it.remark == remark.remark } == null) {
                    existCheck = false
                    return@forEach
                }
            }
        }
        activity.checkAllBtn.isChecked = existCheck
        val font = ResourcesCompat.getFont(activity, R.font.century_gothic)
        activity.checkAllBtn.typeface = font
        activity.checkAllBtn.setOnCheckedChangeListener { buttonView, isChecked ->
            if (!isChecked) {
                activity.checkAllBtn.text = getString(R.string.checkAll)
                map.forEach { entry ->
                    val list = activity.model.newApartmentEntity.progressReview.toMutableList()
                    //это большой костыль, но без него не работает почему-то
                    list.removeAll(entry.listRemark)
                    activity.model.newApartmentEntity.progressReview = list.toMutableSet()
                }
            } else {
                activity.checkAllBtn.text = getString(R.string.uncheckAll)
                map.forEach { entry ->
                    val list = activity.model.newApartmentEntity.progressReview.toMutableList()
                    //это большой костыль, но без него не работает почему-то
                    list.addAll(entry.listRemark)
                    activity.model.newApartmentEntity.progressReview = list.toMutableSet()
                }
            }
            val adapter = mainRecycler?.adapter as AdapterForListRemark?
            adapter?.notifyCheck()
        }
    }

    override fun onStart() {
        super.onStart()
        val listName = mutableListOf<String>()
        map.forEach { listName.add(it.group) }
        loadData(
            listName,
            map,
            type,
            activity.model.newApartmentEntity
        )
    }

    override fun onResume() {
        super.onResume()
        loadData(activity.model.newApartmentEntity)
    }

    private fun loadData(newApartmentEntity: DaoApartmentEntity) = GlobalScope.launch(Dispatchers.Main) {
        withContext(Dispatchers.IO) {
            Repository.getRemarkListFromApartment(newApartmentEntity.id!!) {
                result = it!!
            }
        }
        withContext(Dispatchers.Main) {
            val adapter = mainRecycler?.adapter as AdapterForListRemark?
            adapter?.listDao?.clear()
            adapter?.listDao?.addAll(result)
            adapter?.notifyCheck()
        }
    }


    private fun loadData(
        listName: MutableList<String>,
        map: MutableList<RemarkGroup>,
        type: String,
        newApartmentEntity: DaoApartmentEntity
    ) = GlobalScope.launch(Dispatchers.Main) {
        withContext(Dispatchers.IO) {
            Repository.getRemarkListFromApartment(newApartmentEntity.id!!) {
                result = it!!
            }
        }
        withContext(Dispatchers.Main) {
            mainRecycler?.adapter = AdapterForListRemark(
                listName,
                map,
                type,
                result.toMutableList()
            )
        }
    }
}