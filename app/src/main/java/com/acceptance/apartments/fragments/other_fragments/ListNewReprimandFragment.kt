package com.acceptance.apartments.fragments.other_fragments

import android.view.View
import com.acceptance.apartments.model.DaoApartmentEntity
import kotlinx.android.synthetic.main.fragment_list_reprimand.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ListNewReprimandFragment(
    apartmentEntity: DaoApartmentEntity,
    mainList: Boolean
) : ListReprimandFragment(apartmentEntity, mainList) {
    override fun initNameFragment() {
        titleText.text = "Выявленные замечания и рекомендации"
        textDescription.text = ""
        fr_add_custom_remark_button.visibility = View.GONE
    }

    override fun nextFragment() {
        activity.initFragment(EnterDataFragment(apartmentEntity))
    }

    override fun addDate(data: String) = GlobalScope.launch(Dispatchers.Main) {
        nextFragment()
    }


}