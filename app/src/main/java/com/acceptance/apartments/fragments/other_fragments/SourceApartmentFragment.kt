package com.acceptance.apartments.fragments.other_fragments

import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.children
import androidx.core.view.get
import com.acceptance.apartments.R
import com.acceptance.apartments.adapters.other_adapters.AdapterTypeRemont
import com.acceptance.apartments.adapters.other_adapters.GridAdapter
import com.acceptance.apartments.fragments.AbstractFragment
import com.acceptance.apartments.model.DaoApartmentEntity
import com.acceptance.apartments.model.RemarkGroup
import com.acceptance.apartments.model.Repository
import com.acceptance.apartments.model.TypeApartment.*
import com.acceptance.apartments.model.TypeReview
import com.acceptance.apartments.utils.common.toDate
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.component_botton_of_header.*
import kotlinx.android.synthetic.main.component_header.view.*
import kotlinx.android.synthetic.main.fragment_source_given_apartments.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class SourceApartmentFragment : AbstractFragment(R.layout.fragment_source_given_apartments) {

    lateinit var map: MutableMap<String, MutableList<RemarkGroup>>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRadio()
        removeRadioList()
        initNextBtn()
        initBottomView()
        initIcon()
        initTime()
    }

    private fun initTime() {
        checkBoxOnTime.setOnClickListener {
            checkBoxOnTime.isChecked = true
            editBoxOnTime.setText("", TextView.BufferType.EDITABLE)
        }
        editBoxOnTime.setOnClickListener {
            checkBoxOnTime.isChecked = false
            editBoxOnTime.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus)
                    checkBoxOnTime.isChecked = false
            }
        }
        checkBoxOnTime.isChecked = true
    }

    private fun initIcon() {
        activity.head.iconLayout.setImageDrawable(resources.getDrawable(R.drawable.settings_invisible))
    }

    private fun initBottomView() {
        bottomOfHeader.text = getString(R.string.support_fragment_text)
    }

    private fun initNextBtn() {
        nextBtn.setOnClickListener {
            if (checkOnCorrect()) {
                initNewApartment()

            }
        }
    }

    private fun initNewApartment() {
        val array = resources.getStringArray(R.array.apartmentType)
        val apartmentEntity = DaoApartmentEntity()
        activity.model.newApartmentEntity = apartmentEntity
        apartmentEntity.reviews.add(System.currentTimeMillis().toDate())
        apartmentEntity.rooms.clear()
        var checkBox: RadioButton? = null
        (radio_group.children).forEach { if ((it as RadioButton).isChecked) checkBox = it }
        activity.model.timeOnReview =
            if (checkBoxOnTime.isChecked) 0L else editBoxOnTime.text.toString().toDouble().toInt() * 1000/*to sec*/ * 60 /*to min*/ + System.currentTimeMillis()
        activity.model.timeOnStartReview = if (checkBoxOnTime.isChecked) 0L else System.currentTimeMillis()
        activity.model.isCompleteTime = false
        activity.model.isComplete50PercentTime = false
        activity.model.youLast10Min = false
        activity.model.newApartmentEntity.progressReview.clear()
        when (checkBox!!.text) {
            array[0] -> {
                apartmentEntity.typeApartment = Apartment
                if ((radio_group_balkon.children.toList()[0] as RadioButton).isChecked) {
                    apartmentEntity.rooms.add(getString(R.string.balkon))
                }
                if ((radio_group_kladovka.children.toList()[0] as RadioButton).isChecked)
                    apartmentEntity.rooms.add(getString(R.string.kladovka))
                if ((radio_group_san_usel.children.toList()[0] as RadioButton).isChecked) {
                    apartmentEntity.rooms.add(getString(R.string.split_san_usel))
                } else {
                    apartmentEntity.rooms.add(getString(R.string.vanna))
                    apartmentEntity.rooms.add(getString(R.string.tualet))
                }

                apartmentEntity.rooms.add(getString(R.string.koridor))
                apartmentEntity.rooms.add(getString(R.string.kuhnya))
                try {
                    for (index in 1..(grid.adapter as GridAdapter).checkBox!!.text.toString().toInt()) {
                        apartmentEntity.rooms.add(getString(R.string.room) + " " + index)
                    }
                } catch (ex: NumberFormatException) {
                    apartmentEntity.rooms.add(getString(R.string.room))
                    apartmentEntity.rooms.remove(getString(R.string.kuhnya))
                }
                map = activity.model.listRemarkFromJson
            }
            array[1] -> {
                apartmentEntity.typeApartment = Organization
                for (index in 1..(grid.adapter as GridAdapter).checkBox!!.text.toString().toInt()) {
                    apartmentEntity.rooms.add(getString(R.string.apartment) + " " + index)
                }
                map = activity.model.listRemarkFromJsonCommerce
            }
            array[2] -> {
                apartmentEntity.typeApartment = Parking
                apartmentEntity.typeReview = TypeReview.Clear
                apartmentEntity.rooms.add(getString(R.string.parking_item))
                map = activity.model.listRemarkFromJsonParking
                activity.model.backGroundFragment = Pair(
                    activity.getDrawable(R.drawable.parking),
                    ContextCompat.getColor(activity, R.color.colorParkingBlue)
                )
            }
            else -> {
            }
        }
        loadData(apartmentEntity)
    }

    private fun loadData(apartmentEntity: DaoApartmentEntity) =
        GlobalScope.launch(Dispatchers.Main) {
            withContext(Dispatchers.IO) {
                Repository.setApartment(apartmentEntity)
                Repository.getApartmentList { activity.model.newApartmentEntity = it.last() }
                activity.model.newApartmentEntity.idFirstReview = activity.model.newApartmentEntity.id
                Repository.updateApartment(activity.model.newApartmentEntity)
                Repository.getApartmentList { activity.model.newApartmentEntity = it.last() }
            }
            withContext(Dispatchers.Main) {
                val nextFragment = if (activity.model.newApartmentEntity.typeApartment == Parking) {
                    activity.model.newApartmentEntity.progressReviewAll = activity.model.listRemarkFromJsonParking
                    ListRemarkFragment(
                        activity.model.newApartmentEntity.progressReviewAll[AdapterTypeRemont.arrayUnClear.last()]!!,
                        "Парковочное место"
                    )
                } else TypeRemontFragment(map)
                activity.initFragment(nextFragment)
            }
        }

    private fun checkOnCorrect(): Boolean {
        var result = false
        radio_group.children.forEach { if ((it as RadioButton).isChecked) result = true }

        if (result) {
            val array = resources.getStringArray(R.array.apartmentType)
            val list = booleanArrayOf(false, false, false)

            var checkBox: RadioButton? = null
            (radio_group.children).forEach { if ((it as RadioButton).isChecked) checkBox = it }
            result = when (checkBox!!.text) {
                array[0] -> {
                    radio_group_balkon.children.forEach { if ((it as RadioButton).isChecked) list[0] = true }
                    radio_group_kladovka.children.forEach { if ((it as RadioButton).isChecked) list[1] = true }
                    radio_group_san_usel.children.forEach { if ((it as RadioButton).isChecked) list[2] = true }
                    var res = true
                    if ((grid.adapter as GridAdapter).checkBox == null) res = false
                    list.forEach { if (!it) res = false }
                    res
                }
                array[1] -> (grid.adapter as GridAdapter).checkBox != null
                array[2] -> {
                    true
                }
                else -> false
            }
        }
        if (!checkBoxOnTime.isChecked && editBoxOnTime.text!!.isEmpty()) {
            result = false
        }
        if (!result) Toast.makeText(activity, "Проверьте все поля", Toast.LENGTH_SHORT).show()
        return result
    }

    private fun initRadioList() {
        initCurrentRadio(radio_group_balkon, text_balkon)
        initCurrentRadio(radio_group_kladovka, text_kladovka)
        initCurrentRadio(radio_group_san_usel, text_san_usel)
    }

    private fun removeRadioList() {
        removeCurrentRadio(radio_group_balkon, text_balkon)
        removeCurrentRadio(radio_group_kladovka, text_kladovka)
        removeCurrentRadio(radio_group_san_usel, text_san_usel)
    }

    private fun removeCurrentRadio(radioGroup: RadioGroup, textView: TextView) {
        radioGroup.visibility = GONE
        textView.visibility = GONE
        radioGroup.clearCheck()
    }


    private fun initCurrentRadio(radioGroup: RadioGroup, textView: TextView) {
        radioGroup.removeAllViews()
        radioGroup.visibility = VISIBLE
        textView.visibility = VISIBLE
        val array = resources.getStringArray(R.array.answerForRadio)
        array.forEach { text ->
            val rb = RadioButton(activity)
            rb.text = text
            val font = ResourcesCompat.getFont(activity, R.font.century_gothic)
            rb.typeface = font
            radioGroup.addView(rb)
        }
        radioGroup.check(radioGroup[0].id)
    }

    private fun initRadio() {
        radio_group.removeAllViews()
        val array = resources.getStringArray(R.array.apartmentType)
        array.forEach { text ->
            val rb = RadioButton(activity)
            rb.text = text
            val font = ResourcesCompat.getFont(activity, R.font.century_gothic)
            rb.typeface = font
            radio_group.addView(rb)
            rb.setOnClickListener {
                timeOnView.visibility = VISIBLE
                when (text) {
                    array[0] -> {
                        divider1.visibility = VISIBLE
                        divider2.visibility = VISIBLE
                        divider3.visibility = VISIBLE
                        grid.adapter =
                            GridAdapter(6, listOf(null, null, null, null, null, "Студия"))
                        grid.numColumns = 3
                        gridName.text = getString(R.string.number_live_room)
                        initRadioList()
                        activity.model.newApartmentEntity.typeApartment = Apartment
                    }
                    array[1] -> {
                        divider1.visibility = VISIBLE
                        divider2.visibility = VISIBLE
                        divider3.visibility = GONE
                        grid.adapter = GridAdapter(10)
                        grid.numColumns = 5
                        gridName.text = getString(R.string.number_includ_room)
                        removeRadioList()
                        activity.model.newApartmentEntity.typeApartment = Organization
                    }
                    array[2] -> {
                        divider1.visibility = VISIBLE
                        divider2.visibility = GONE
                        divider3.visibility = GONE
                        grid.adapter = GridAdapter(0)
                        gridName.text = ""
                        removeRadioList()
                        timeOnView.visibility = GONE
                        checkBoxOnTime.isChecked = true
                        activity.model.newApartmentEntity.typeApartment = Parking
                    }
                }
            }
        }
    }
}