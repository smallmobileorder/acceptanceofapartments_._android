package com.acceptance.apartments.fragments.other_fragments

import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.acceptance.apartments.R
import com.acceptance.apartments.adapters.other_adapters.AdapterTypeWork
import com.acceptance.apartments.fragments.AbstractFragment
import com.acceptance.apartments.model.RemarkGroup
import com.acceptance.apartments.model.TypeApartment
import kotlinx.android.synthetic.main.component_btn_back.*
import kotlinx.android.synthetic.main.component_progress_bar.view.*
import kotlinx.android.synthetic.main.fragment_type_work.*
import java.util.*
import kotlin.concurrent.thread


class TypeWorkingFragment(private val listRemark: MutableMap<String, MutableList<RemarkGroup>> = mutableMapOf()) :
    AbstractFragment(R.layout.fragment_type_work) {

    lateinit var adapter: AdapterTypeWork

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val list = resources.getStringArray(R.array.workType).toList().subList(0, 7)
        initBtns()
        initProgress()
        val enableViewList = Array(8) { true }
        if (!activity.model.newApartmentEntity.rooms.contains(getString(R.string.balkon))) {
            enableViewList[3] = false
            enableViewList[4] = false
        }
        if (activity.model.newApartmentEntity.typeApartment == TypeApartment.Organization) {
            enableViewList[3] = true
        }

        adapter = AdapterTypeWork(
            list,
            containerView,
            enableViewList.toList(),
            Array(8) { false }.toList(),
            resources.getStringArray(R.array.backgroundList).toList(),
            resources.getStringArray(R.array.iconList).toList(),
            listRemark,
            resources.getStringArray(R.array.iconListInvisible).toList()
        )
    }


    private fun initProgress() {
        progressTime.nameProgress.text = "Время"
        progressTime.progress_horizontal.progressDrawable =
            resources.getDrawable(R.drawable.progress_drawable_time)
        progressProgress.nameProgress.text = "Прогресс"
        progressProgress.progress_horizontal.progressDrawable =
            resources.getDrawable(R.drawable.progress_drawable_progress)
        if (activity.model.timeOnReview != 0L) {
            reScheduleTimer()
        } else {
            progressTime.visibility = View.GONE
        }
    }

    private fun initBtns() {
        save_btn.setOnClickListener {
            activity.initFragment(ResultReviewProgressFragment(activity.model.newApartmentEntity, true))
        }
        btnBack.setOnClickListener { activity.onBackPressed() }
    }

    private var timer = Timer()
    private var timerTask = MyTimerTask()
    private var progress = 0
    private var working = false
    private fun reScheduleTimer() {
        working = true
        timer = Timer()
        timerTask = MyTimerTask()
        timer.schedule(timerTask, 0, 500)
    }

    override fun onResume() {
        super.onResume()
        progress = (activity.model.newApartmentEntity.progressReview.size * 100.0 / activity.model.newApartmentEntity.progressReviewCount).toInt()
        progressProgress.progress_horizontal?.progress = progress
        save_btn?.isEnabled = progress > 99.9
        if (!working) reScheduleTimer()
        thread {

            Thread.sleep(110)
            activity.runOnUiThread { adapter.notifyDataChanged() }
        }
    }

    override fun onStop() {
        super.onStop()
        timerTask.cancel()
        timer.cancel()
        working = false
    }


    private inner class MyTimerTask : TimerTask() {
        override fun run() {
            if (activity.model.timeOnReview != 0L) {
                val progress = ((System.currentTimeMillis() - activity.model.timeOnStartReview).toDouble() /
                        (activity.model.timeOnReview - activity.model.timeOnStartReview) * 100).toInt()
                progressTime?.progress_horizontal?.progress = progress
                if (progress >= 50 && !activity.model.isComplete50PercentTime) {
                    activity.model.isComplete50PercentTime = true
                    activity.runOnUiThread {
                        Toast.makeText(
                            context,
                            "Внимание. Прошла половина отведённого на осмотр времени",
                            Toast.LENGTH_LONG
                        )
                            .show()
                    }
                }
                if ((activity.model.timeOnReview - activity.model.timeOnStartReview) / 1000 / 60 < 10) {
                    activity.model.youLast10Min = true
                }
                if ((activity.model.timeOnReview - System.currentTimeMillis()) / 1000 / 60 < 10 && !activity.model.youLast10Min) {
                    activity.model.youLast10Min = true
                    activity.runOnUiThread {
                        Toast.makeText(
                            context,
                            "Внимание. Осталось 10 минут отведённого на осмотр времени",
                            Toast.LENGTH_LONG
                        )
                            .show()
                    }
                }
                if (progress >= 99 && !activity.model.isCompleteTime) {
                    activity.model.isCompleteTime = true
                    activity.runOnUiThread {
                        Toast.makeText(
                            context,
                            "Внимание. Закончилось основное отведённое на осмотр время",
                            Toast.LENGTH_LONG
                        )
                            .show()
                    }
                }
            }
        }
    }

}