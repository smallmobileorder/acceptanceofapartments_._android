package com.acceptance.apartments.fragments.other_fragments

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.acceptance.apartments.R
import com.acceptance.apartments.adapters.other_adapters.AdapterForProgressItemInResult
import com.acceptance.apartments.fragments.AbstractFragment
import com.acceptance.apartments.model.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.component_header.view.*
import kotlinx.android.synthetic.main.component_recycler_view.*
import kotlinx.android.synthetic.main.fragment_result_review_progress.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ResultReviewProgressFragment(
    private val apartmentEntity: DaoApartmentEntity,
    private val isMainList: Boolean = true,
    private val fromListReview: Boolean = false
) :
    AbstractFragment(R.layout.fragment_result_review_progress) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        apartmentEntity.progressIsComplete = true
        activity.head.iconLayout.setImageDrawable(resources.getDrawable(R.drawable.user_invisible))
        loadData()
    }


    private fun initShowRemarkBtn() {
        btnShowRemark.setOnClickListener {
            activity.initFragment(
                when {
                    fromListReview -> ListResultReviewFragment(
                        apartmentEntity
                    )
                    isMainList -> ListNewReprimandFragment(
                        apartmentEntity,
                        isMainList
                    )
                    else -> ListReprimandFragment(
                        apartmentEntity,
                        isMainList
                    )
                }
            )

        }
    }

    private fun loadData() = GlobalScope.launch(Dispatchers.Main) {
        var remarkList = listOf<DaoRemarkEntity>()
        val map = mutableMapOf<String, Int>()
        var resultCount = 1
        withContext(Dispatchers.IO) {
            Repository.getRemarkListFromApartment(apartmentEntity.id!!) {
                remarkList = it!!.filter { remark -> remark.isShow }
            }
            remarkList.forEach {
                val value = it.typeRemark
                if (map.contains(value)) {
                    map[value] = map[value]!! + 1
                } else map[value] = 1
            }
            var currentProgress = 0.0
            remarkList.forEach {
                currentProgress += it.levelCritic
            }
            val typeReview = apartmentEntity.typeReview
            val typeApartment = apartmentEntity.typeApartment
            val allRemarkCountCritic = when {
                typeReview == TypeReview.Clear && typeApartment == TypeApartment.Apartment -> 1698
                typeReview == TypeReview.Clear && typeApartment == TypeApartment.Organization -> 888
                typeReview == TypeReview.Clear && typeApartment == TypeApartment.Parking -> 134
                typeReview == TypeReview.Preclear && typeApartment == TypeApartment.Apartment -> 1111
                typeReview == TypeReview.Preclear && typeApartment == TypeApartment.Organization -> 888
                typeReview == TypeReview.Preclear && typeApartment == TypeApartment.Parking -> 134
                typeReview == TypeReview.Unclear && typeApartment == TypeApartment.Apartment -> 995
                typeReview == TypeReview.Unclear && typeApartment == TypeApartment.Organization -> 783
                typeReview == TypeReview.Unclear && typeApartment == TypeApartment.Parking -> 134
                else -> 0
            }
            resultCount = (100.0 - currentProgress / allRemarkCountCritic * 100.0).toInt()
            remarkList.forEach {
                if (it.levelCritic == 10) resultCount -= 30
            }
            if (resultCount < 0) resultCount = 0
            if (resultCount > 100) resultCount = 100
        }
        withContext(Dispatchers.Main) {
            countSuccess.text = resultCount.toString()
            countSuccess.setTextColor(
                ContextCompat.getColor(
                    activity, when (resultCount) {
                        in 90..100 -> R.color.color100_84
                        in 80..89 -> R.color.color83_67
                        in 70..79 -> R.color.color66_50
                        else -> R.color.color49__
                    }
                )
            )
            val max = map.values.toList().max()
            if (max != null) mainRecycler?.adapter = AdapterForProgressItemInResult(
                map.keys.toList(),
                map,
                max
            )
            if (map.isEmpty()) {
                btnShowRemark.text = "Далее"
                btnShowRemark.setOnClickListener { activity.initFragment(EnterDataFragment(apartmentEntity)) }
            } else {
                initShowRemarkBtn()
            }
        }
    }
}