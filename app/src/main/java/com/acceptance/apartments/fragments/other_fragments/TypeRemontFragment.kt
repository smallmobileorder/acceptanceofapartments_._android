package com.acceptance.apartments.fragments.other_fragments

import android.os.Bundle
import android.view.View
import com.acceptance.apartments.R
import com.acceptance.apartments.adapters.other_adapters.AdapterTypeRemont
import com.acceptance.apartments.fragments.AbstractFragment
import com.acceptance.apartments.model.RemarkGroup
import com.acceptance.apartments.model.TypeApartment
import kotlinx.android.synthetic.main.component_botton_of_header.*
import kotlinx.android.synthetic.main.component_recycler_view.*

class TypeRemontFragment(var map: MutableMap<String, MutableList<RemarkGroup>>) :
    AbstractFragment(R.layout.fragment_type_remont) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val list = resources.getStringArray(R.array.variantRemont).toMutableList()
        if (activity.model.newApartmentEntity.typeApartment == TypeApartment.Organization) {
            list.removeAt(0)
        }
        mainRecycler?.adapter = AdapterTypeRemont(list, map)
        initBottomView()
    }

    private fun initBottomView() {
        bottomOfHeader.text = getString(R.string.type_remont_fragment_text)
        bottomOfHeader.textSize = 20f
    }

}