package com.acceptance.apartments.fragments.other_fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.acceptance.apartments.R
import com.acceptance.apartments.adapters.other_adapters.AdapterListReprimand
import com.acceptance.apartments.fragments.AbstractFragment
import com.acceptance.apartments.model.DaoApartmentEntity
import com.acceptance.apartments.model.DaoRemarkEntity
import com.acceptance.apartments.model.Repository
import com.acceptance.apartments.model.TypeApartment
import com.acceptance.apartments.utils.common.toDate
import kotlinx.android.synthetic.main.component_recycler_view.*
import kotlinx.android.synthetic.main.fragment_list_reprimand.*
import kotlinx.coroutines.*


open class ListReprimandFragment(
    var apartmentEntity: DaoApartmentEntity,
    open val isMainList: Boolean = false
) :
    AbstractFragment(R.layout.fragment_list_reprimand) {
    var listRemark = mutableListOf<DaoRemarkEntity>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initBottomView()
        initButtonBack()
        initButtonSave()
        initAddNewRemarkBtn()
        initNameFragment()
        updateStateSaveBtn()
        mainRecycler?.adapter = AdapterListReprimand(
            listOf(),
            listRemark,
            this@ListReprimandFragment, isMainList
        )
    }

    private fun initAddNewRemarkBtn() {
        fr_add_custom_remark_button.setOnClickListener {
            activity.model.newApartmentEntity = apartmentEntity
            activity.model.isCustomRemark = true
            activity.model.backGroundFragment = Pair(
                activity.getDrawable(R.drawable.icon_new),
                ContextCompat.getColor(activity, R.color.colorPrimaryDarkItem)
            )
            activity.initFragment(NewRemarkFragment(type = "Новое"))
        }
    }

    override fun onResume() {
        println("onResume")
        super.onResume()
        loadData()
        println(activity.model.newApartmentEntity.id)
    }


    private fun createCopyApartment() {
        println("createCopyApartment")
        val newApartmentEntity = apartmentEntity.copy(id = null)
        newApartmentEntity.idFirstReview = apartmentEntity.idFirstReview ?: apartmentEntity.id
        newApartmentEntity.reviewIsComplete = false
        Repository.setApartment(newApartmentEntity)
        var previoslyListRemark = listOf<DaoRemarkEntity>()
        Repository.getRemarkListFromApartment(apartmentEntity.id!!) { previoslyListRemark = it!! }
        Repository.getApartmentList { apartmentEntity = it.last() }
        previoslyListRemark.forEach {
            val newRemark = it.copy(id = null, apartmentID = apartmentEntity.id)
            Repository.setRemark(newRemark)
        }
    }

    private fun loadData() = GlobalScope.launch(Dispatchers.Main) {
        println("loadData")
        withContext(Dispatchers.IO) {
            if (!isMainList) {
                createCopyApartment()
                activity.model.newApartmentEntity = apartmentEntity
            }
            Repository.getRemarkListFromApartment(apartmentEntity.id!!) {
                listRemark.addAll(it!!)
            }
            val rooms = mutableListOf("Все комнаты")
            rooms.addAll(apartmentEntity.rooms)
            initSpinner(rooms.toMutableSet().toList())
        }
    }

    private fun initSpinner(rooms: List<String>) {
        println("initSpinner")
        val dataAdapter = ArrayAdapter(
            context!!,
            android.R.layout.simple_spinner_item, rooms
        )
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        activity.runOnUiThread {
            spinner?.adapter = dataAdapter
        }
        spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                println("onItemSelected")
                updateList(rooms[position])
            }
        }
        if (spinner == null) updateList("Все комнаты")
    }

    fun updateList(room: String) = GlobalScope.launch(Dispatchers.Main) {
        val adapter = mainRecycler?.adapter as AdapterListReprimand?
        withContext(Dispatchers.IO) {
            listRemark.clear()
            if (room == "Все комнаты")
                Repository.getRemarkListFromApartment(apartmentEntity.id!!) {
                    listRemark.addAll(if (isMainList) it!! else it!!.filter { remark -> remark.isShow })
                }
            else {
                Repository.getRemarkListFromApartmentFromRoom(apartmentEntity.id!!, room) {
                    listRemark.addAll(if (isMainList) it!! else it!!.filter { remark -> remark.isShow })
                }
            }
            val listName = Array(listRemark.size) { "" }
            adapter?.list = listName.toList()
            adapter?.listRemark = listRemark
        }
        withContext(Dispatchers.Main) {
            println("" + apartmentEntity.id + "  --- " + listRemark)
            mainRecycler?.adapter?.notifyDataSetChanged()
        }
    }


    private fun initButtonBack() {
        btnBack.setOnClickListener { activity.onBackPressed() }
    }

    private fun initButtonSave() {
        save_btn.setOnClickListener {
            addDate(System.currentTimeMillis().toDate())
        }
    }

    fun updateStateSaveBtn() {
        var flag = true
        if (!isMainList)
            GlobalScope.launch(Dispatchers.Main) {
                withContext(Dispatchers.IO) {
                    delay(100)
                    Repository.getRemarkListFromApartment(apartmentEntity.id!!) {
                        flag = it!!.none { remark -> remark.isShow && remark.isComplete == null }
                    }
                }
                save_btn.isEnabled = flag
            } else {
            save_btn.isEnabled = flag
        }
    }

    private fun initBottomView() {
        headerView.text = getString(R.string.choose_room)
    }

    open fun addDate(data: String) = GlobalScope.launch(Dispatchers.Main) {
        withContext(Dispatchers.IO) {
            apartmentEntity.reviewIsComplete = true
            apartmentEntity.reviews.add(data)
            Repository.setApartment(apartmentEntity)
        }
        withContext(Dispatchers.Main) {
            Toast.makeText(context, "Данные обновлены", Toast.LENGTH_SHORT).show()
            nextFragment()
        }
    }

    open fun nextFragment() {
        activity.initFragment(ResultSecondReviewProgressFragment(apartmentEntity))
    }

    @SuppressLint("SetTextI18n")
    open fun initNameFragment() {
        textDescription.text =
            "Замечания по " + when (apartmentEntity.typeApartment) {
                TypeApartment.Apartment -> "квартире"
                TypeApartment.Organization -> "нежилому помещению"
                else -> "парковочному месту"
            } + " №" + apartmentEntity.numberRoom +
                    ", выявленные на осмотре от " + apartmentEntity.reviews.last()
    }
}