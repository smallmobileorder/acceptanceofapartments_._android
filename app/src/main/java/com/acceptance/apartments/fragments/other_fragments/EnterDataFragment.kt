package com.acceptance.apartments.fragments.other_fragments

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import com.acceptance.apartments.R
import com.acceptance.apartments.fragments.AbstractFragment
import com.acceptance.apartments.model.DaoApartmentEntity
import com.acceptance.apartments.model.Repository
import com.google.android.material.textfield.TextInputEditText
import kotlinx.android.synthetic.main.component_edit_text_consumer.view.*
import kotlinx.android.synthetic.main.fragment_for_enter_data.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*


open class EnterDataFragment(private var apartmentEntity: DaoApartmentEntity) :
    AbstractFragment(R.layout.fragment_for_enter_data) {
    val listConsumer: MutableList<TextInputEditText> = mutableListOf()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addNew()
        initButtonSkip()
        initButtonCreateReport()
        initAddUserBtn()
        initDataContract()
    }

    private fun initDataContract() {
        dataContractEdit.setOnClickListener { showCalendar() }
        dataContractEdit.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                showCalendar()
            }
        }
    }

    @SuppressLint("SimpleDateFormat", "SetTextI18n")
    private fun showCalendar() {
        val dateFormat = SimpleDateFormat("yyyy/MM/dd")
        val date = Date()
        val currentData = dateFormat.format(date).split("/")
        val startTime = DatePickerDialog(
            activity,
            DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                val data = "$dayOfMonth." +
                        (month + 1) + ".$year"
                val f = SimpleDateFormat("dd.MM.yyyy")
                val d = f.parse(data)
                val milliseconds = d.time
                val normData = SimpleDateFormat("dd.MM.yyyy").format(Date(milliseconds))!!
                dataContractEdit.setText(
                    normData
                )
            },
            currentData[0].toInt(),
            currentData[1].toInt() - 1,
            currentData[2].toInt()
        )
        startTime.show()
        initListener()
    }

    private fun initListener() {
        val watcher = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                btnCreateReport.isEnabled = !checkOnCorrect()
            }
        }
        addressObjectEdit.addTextChangedListener(watcher)
        nameLiveBuildingEdit.addTextChangedListener(watcher)
        dataContractEdit.addTextChangedListener(watcher)
        numberContractEdit.addTextChangedListener(watcher)
        listConsumer.first().addTextChangedListener(watcher)
        numberRoomEdit.addTextChangedListener(watcher)
        contactNumberPhoneEdit.addTextChangedListener(watcher)
    }

    private fun initAddUserBtn() {
        addUserBtn.setOnClickListener {
            addNew()
        }

    }

    private fun addNew() {
        val view = LayoutInflater.from(context).inflate(R.layout.component_edit_text_consumer, null)
        linerData.addView(view, 1)
        listConsumer.add(view.fullNameEdit)
    }


    open fun updateData() = GlobalScope.launch(Dispatchers.Main) {
        withContext(Dispatchers.IO) {
            Repository.setApartment(apartmentEntity)
            Repository.getApartmentList { apartmentEntity = it.last() }
        }
        withContext(Dispatchers.Main) {
            Toast.makeText(context, "Данные обновлены", Toast.LENGTH_SHORT).show()
            activity.initFragment(ListResultReviewFragment(apartmentEntity))
        }
    }


    private fun initButtonSkip() {
        btnSkip.setOnClickListener {
            updateData()
        }
    }

    private fun checkList(): Boolean {
        var flag = false
        listConsumer.forEach { if (!it.text.isNullOrEmpty()) flag = true }
        return flag
    }

    private fun checkNumber(): Boolean {
        return if (TextUtils.isEmpty(contactNumberPhoneEdit.text)) {
            false
        } else {
            android.util.Patterns.PHONE.matcher(contactNumberPhoneEdit.text).matches()
        }
    }


    private fun checkOnCorrect(): Boolean = addressObjectEdit.text.toString().isEmpty() ||
            nameLiveBuildingEdit.text.toString().isEmpty() ||
            dataContractEdit.text.toString().isEmpty() ||
            numberContractEdit.text.toString().isEmpty() ||
            !checkList() ||
            !checkNumber() ||
            numberRoomEdit.text.toString().isEmpty() ||
            contactNumberPhoneEdit.text.toString().isEmpty()

    private fun initButtonCreateReport() {
        btnCreateReport.setOnClickListener {
            if (checkOnCorrect()) {
                Toast.makeText(context, getString(R.string.not_all_field_exist), Toast.LENGTH_SHORT).show()
            } else {
                apartmentEntity.address = addressObjectEdit.text.toString()
                apartmentEntity.description = nameLiveBuildingEdit.text.toString()
                apartmentEntity.dataContract = dataContractEdit.text.toString()
                apartmentEntity.numberContract = numberContractEdit.text.toString()
                listConsumer.forEach {
                    if (!it.text.isNullOrEmpty()) {
                        apartmentEntity.fullName.add(it.text.toString())
                    }
                }
                apartmentEntity.numberRoom = numberRoomEdit.text.toString()
                apartmentEntity.numberPhone = contactNumberPhoneEdit.text.toString()
                updateData()
            }
        }
    }

}