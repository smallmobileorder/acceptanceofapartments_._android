package com.acceptance.apartments.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.acceptance.apartments.MainActivity
import com.acceptance.apartments.MyViewModel
import kotlinx.android.synthetic.main.activity_main.*

abstract class AbstractFragment(private val layoutID: Int) : Fragment() {

    lateinit var activity: MainActivity

    private lateinit var model: MyViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = getActivity() as MainActivity
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(layoutID, container, false)!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        model = ViewModelProviders.of(activity).get(MyViewModel::class.java)
    }

    override fun onResume() {
        super.onResume()


        activity.progressView.visibility = View.GONE
    }
}