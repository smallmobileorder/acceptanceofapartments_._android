package com.acceptance.apartments.fragments.other_fragments

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.acceptance.apartments.R
import com.acceptance.apartments.adapters.other_adapters.AdapterForListRemarkExcel
import com.acceptance.apartments.fragments.AbstractFragment
import com.acceptance.apartments.model.DaoApartmentEntity
import com.acceptance.apartments.model.DaoRemarkEntity
import com.acceptance.apartments.model.Repository
import com.acceptance.apartments.model.TypeApartment
import kotlinx.android.synthetic.main.component_recycler_view.*
import kotlinx.android.synthetic.main.fragment_export_to_excel.*
import kotlinx.android.synthetic.main.fragment_export_to_excel.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.apache.poi.hssf.usermodel.HSSFFont
import org.apache.poi.ss.usermodel.CellStyle
import org.apache.poi.ss.usermodel.IndexedColors
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.util.CellRangeAddress
import org.apache.poi.ss.util.CellUtil
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import java.io.File
import java.net.URLConnection


open class ListResultReviewFragment(
    private val apartmentEntity: DaoApartmentEntity,
    val layout: Int = R.layout.fragment_export_to_excel
) :
    AbstractFragment(layout) {

    private val WRITE_CODE: Int = 1234
    val map = mutableMapOf<String, List<DaoRemarkEntity>>()
    val fullMap = mutableMapOf<String, List<DaoRemarkEntity>>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.btnExport.setOnClickListener {
            clickListener()
        }
        view.btnComplete.setOnClickListener {
            activity.initFragment(MenuFragment())
        }
        loadData()
    }

    fun clickListener() {
        val cameraPermissionResult =
            ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (cameraPermissionResult != PackageManager.PERMISSION_GRANTED) {
            val permissions = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            ActivityCompat.requestPermissions(activity, permissions, WRITE_CODE)
        } else {
            sendData()
        }
    }

    open fun sendData() = GlobalScope.launch(Dispatchers.Main) {
        withContext(Dispatchers.Main) {
            Toast.makeText(context, "Обработка данных", Toast.LENGTH_LONG).show()
            btnExport?.isEnabled = false
        }
        val myExternalFile = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "table1.xlsx"
        )
        withContext(Dispatchers.IO) {
            writeDateToExcel(myExternalFile)
        }
        withContext(Dispatchers.Main) {
            val builder = StrictMode.VmPolicy.Builder()
            StrictMode.setVmPolicy(builder.build())
            val intentShareFile = Intent(Intent.ACTION_SEND)
            intentShareFile.type = URLConnection.guessContentTypeFromName(myExternalFile.name)
            intentShareFile.putExtra(
                Intent.EXTRA_STREAM,
                Uri.parse("file://" + myExternalFile.absolutePath)
            )
            activity.startActivity(
                Intent.createChooser(
                    intentShareFile,
                    "Share File"
                )
            )
            btnExport.isEnabled = true
        }
    }

    private fun writeDateToExcel(myExternalFile: File) {

        val xlWb = XSSFWorkbook()
        val xlWs = xlWb.createSheet()
        val header = xlWs.footer
        header.center = "Отчет сформирован через мобильное приложение \"Приёмка квартиры\""
        xlWs.setColumnWidth(0, 389 * 3)
        xlWs.setColumnWidth(1, 1344 * 3)
        xlWs.setColumnWidth(2, 1167 * 3)
        xlWs.setColumnWidth(3, 1478 * 3)
        xlWs.setColumnWidth(4, 1450 * 3)
        xlWs.setColumnWidth(5, 1450 * 3)

        val boldBigStyle = xlWb.createCellStyle()
        val boldBigFont = xlWb.createFont()
        boldBigStyle.wrapText = true
        boldBigFont.bold = true
        boldBigFont.fontHeightInPoints = 14
        boldBigStyle.setFont(boldBigFont)

        val boldDefaultStyle = xlWb.createCellStyle()
        boldDefaultStyle.wrapText = true
        val boldDefaultFont = xlWb.createFont()
        boldDefaultFont.bold = true
        boldDefaultFont.fontHeightInPoints = 11
        boldDefaultStyle.setFont(boldDefaultFont)

        val boldUnderlineStyle = xlWb.createCellStyle()
        boldUnderlineStyle.wrapText = true
        val boldUnderlineFont = xlWb.createFont()
        boldUnderlineFont.bold = true
        boldUnderlineFont.fontHeightInPoints = 11
        boldUnderlineFont.underline = HSSFFont.U_SINGLE
        boldUnderlineStyle.setFont(boldUnderlineFont)

        val italicStyle = xlWb.createCellStyle()
        italicStyle.wrapText = true
        val italicFont = xlWb.createFont()
        italicFont.italic = true
        italicFont.fontHeightInPoints = 11
        italicStyle.setFont(italicFont)

        val defaultStyle = xlWb.createCellStyle()
        defaultStyle.wrapText = true
        val defaultFont = xlWb.createFont()
        defaultFont.bold = false
        defaultFont.fontHeightInPoints = 11
        defaultStyle.setFont(defaultFont)


        var currentIndex = 0
        val cellTitleRegion = CellRangeAddress(currentIndex, currentIndex, 0, 5)
        val cellTitle = xlWs.createRow(cellTitleRegion.firstRow).createCell(cellTitleRegion.firstColumn)
        xlWs.addMergedRegion(cellTitleRegion)
        cellTitle.setCellValue(
            "Приложение к Акту осмотра " + when (apartmentEntity.typeApartment) {
                TypeApartment.Apartment -> "квартиры"
                TypeApartment.Organization -> "нежилого помещения"
                else -> "парковочного места"
            } + " №" + apartmentEntity.numberRoom +
                    " от " + apartmentEntity.reviews.first()
        )
        cellTitle.cellStyle = boldBigStyle
        CellUtil.setAlignment(cellTitle, xlWb, CellStyle.ALIGN_CENTER)
        currentIndex++


        val cellReportRegion = CellRangeAddress(currentIndex, currentIndex, 0, 5)
        xlWs.addMergedRegion(cellReportRegion)
        val cellReport = xlWs.createRow(cellReportRegion.firstRow).createCell(cellReportRegion.firstColumn)
        cellReport.setCellValue(
            "по Договору " + apartmentEntity.numberContract + " от " + apartmentEntity.dataContract
        )
        cellReport.cellStyle = boldBigStyle
        CellUtil.setAlignment(cellReport, xlWb, CellStyle.ALIGN_CENTER)
        currentIndex++



        if (apartmentEntity.reviews.size > 1) {
            currentIndex++
            val cellFirstReviewRange = CellRangeAddress(currentIndex, currentIndex, 0, 5)
            xlWs.addMergedRegion(cellFirstReviewRange)
            val cellFirstReview =
                xlWs.createRow(cellFirstReviewRange.firstRow).createCell(cellFirstReviewRange.firstColumn)
            cellFirstReview.setCellValue(
                "Повторный осмотр от " + apartmentEntity.reviews.last()
            )
            cellFirstReview.cellStyle = boldBigStyle
            CellUtil.setAlignment(cellFirstReview, xlWb, CellStyle.ALIGN_CENTER)
            currentIndex++
        }


        val cellConsumerRegion = CellRangeAddress(currentIndex, currentIndex, 0, 5)
        xlWs.addMergedRegion(cellConsumerRegion)
        val cellConsumers = xlWs.createRow(cellConsumerRegion.firstRow).createCell(cellConsumerRegion.firstColumn)
        var resultStr = ""
        apartmentEntity.fullName.forEachIndexed { index, s ->
            resultStr += s
            if (index != apartmentEntity.fullName.lastIndex) resultStr += ", "
        }
        cellConsumers.setCellValue(
            "Дольщик(и): $resultStr"
        )
        cellConsumers.row.height = -1
        cellConsumers.cellStyle = boldDefaultStyle
        currentIndex++

        val cellPhoneRange = CellRangeAddress(currentIndex, currentIndex, 0, 5)
        xlWs.addMergedRegion(cellPhoneRange)
        val cellPhone = xlWs.createRow(cellPhoneRange.firstRow).createCell(cellPhoneRange.firstColumn)
        cellPhone.setCellValue(
            "Телефон для связи: " + apartmentEntity.numberPhone
        )
        cellPhone.cellStyle = italicStyle
        currentIndex++
        currentIndex++

        val cellDescriptionRange = CellRangeAddress(currentIndex, currentIndex, 0, 5)
        xlWs.addMergedRegion(cellDescriptionRange)
        val cellDescription = xlWs.createRow(cellDescriptionRange.firstRow).createCell(cellDescriptionRange.firstColumn)
        var containsComplete = false
        var containsNotComplete = false
        fullMap.forEach { entry ->
            entry.value.forEach {
                if (it.isComplete == true) containsComplete = true
                else containsNotComplete = true
            }
        }

        cellDescription.setCellValue(
            if (apartmentEntity.reviews.size == 1) {
                "При осмотре " +
                        when (apartmentEntity.typeApartment) {
                            TypeApartment.Apartment -> "квартиры"
                            TypeApartment.Organization -> "нежилого помещения"
                            else -> "парковочного места"
                        } + " №" + apartmentEntity.numberRoom +
                        " в ЖК \"" + apartmentEntity.description +
                        "\", расположенном по адресу : \"" +
                        apartmentEntity.address +
                        "\", дольщиком были выявлены следующие недостатки:"
            } else {
                "Замечания по " +
                        when (apartmentEntity.typeApartment) {
                            TypeApartment.Apartment -> "квартиры"
                            TypeApartment.Organization -> "нежилому помещению"
                            else -> "парковочному месту"
                        } + " №" + apartmentEntity.numberRoom +
                        ", выявленные на осмотре от  " + apartmentEntity.reviews.first() +
                        when {
                            !containsComplete && containsNotComplete -> " не устранено"
                            containsComplete && containsNotComplete -> " устранены частично"
                            else -> " устранены в полном объеме"
                        } + "."
            }
        )
        cellDescription.cellStyle = defaultStyle
        cellDescription.row.height = (cellTitle.row.height * 2).toShort()
        currentIndex++

        if (fullMap.isNotEmpty()) writeHeaderForRemarkList(xlWs, currentIndex)
        currentIndex++

        currentIndex = writeRemarkToExcel(xlWs, currentIndex)
        currentIndex++

        val cellConsumersRow = xlWs.createRow(currentIndex)
        val cellConsumersList = cellConsumersRow.createCell(1)
        cellConsumersList.setCellValue("Дольщик(и):")
        cellConsumersList.cellStyle = boldUnderlineStyle

        val cellBuilderList = cellConsumersRow.createCell(4)
        cellBuilderList.setCellValue("Застройщик:")
        cellBuilderList.cellStyle = boldUnderlineStyle
        currentIndex++
        currentIndex++

        writeConsumerToExcel(xlWs, currentIndex)

        xlWb.write(myExternalFile.outputStream())
        xlWb.close()
    }

    private fun writeHeaderForRemarkList(xlWs: Sheet, currentIndex: Int) {
        val defaultStyle = xlWs.workbook.createCellStyle()
        defaultStyle.wrapText = true
        defaultStyle.verticalAlignment = 1
        val defaultFont = xlWs.workbook.createFont()
        defaultFont.bold = true
        defaultFont.fontHeightInPoints = 11
        defaultStyle.setFont(defaultFont)
        val thin = 1.toShort()
        val black = IndexedColors.BLACK.getIndex()
        defaultStyle.borderTop = thin
        defaultStyle.borderBottom = thin
        defaultStyle.borderRight = thin
        defaultStyle.borderLeft = thin
        defaultStyle.topBorderColor = black
        defaultStyle.rightBorderColor = black
        defaultStyle.bottomBorderColor = black
        defaultStyle.leftBorderColor = black
        var startRow = currentIndex
        val row = xlWs.createRow(startRow)

        var cell = row.createCell(0)
        cell.cellStyle = defaultStyle
        cell.setCellValue("№")

        cell = row.createCell(1)
        cell.cellStyle = defaultStyle
        cell.setCellValue("Помещение")

        cell = row.createCell(2)
        cell.cellStyle = defaultStyle
        cell.setCellValue("Вид работ")

        val cellDescriptionRange =
            CellRangeAddress(startRow, startRow, 3, if (layout == R.layout.fragment_export_to_excel) 5 else 4)
        xlWs.addMergedRegion(cellDescriptionRange)
        cell = row.createCell(3)
        cell.cellStyle = defaultStyle
        cell.setCellValue("Замечание")
        cell = row.createCell(4)
        cell.cellStyle = defaultStyle
        cell = row.createCell(5)
        cell.cellStyle = defaultStyle
        if (layout != R.layout.fragment_export_to_excel) {
            cell.setCellValue("Устранено")
        }
    }

    fun writeRemarkToExcel(xlWs: Sheet, currentIndex: Int): Int {
        val defaultStyle = xlWs.workbook.createCellStyle()
        defaultStyle.wrapText = true
        defaultStyle.verticalAlignment = 1
        val defaultFont = xlWs.workbook.createFont()
        defaultFont.bold = false
        defaultFont.fontHeightInPoints = 11
        defaultStyle.setFont(defaultFont)
        val thin = 1.toShort()
        val black = IndexedColors.BLACK.getIndex()
        defaultStyle.borderTop = thin
        defaultStyle.borderBottom = thin
        defaultStyle.borderRight = thin
        defaultStyle.borderLeft = thin
        defaultStyle.topBorderColor = black
        defaultStyle.rightBorderColor = black
        defaultStyle.bottomBorderColor = black
        defaultStyle.leftBorderColor = black
        var startRow = currentIndex
        var indexInList = 1
        val wrapStyle = xlWs.workbook.createCellStyle()
        wrapStyle.wrapText = true
        fullMap.forEach { entry ->
            var row: Row
            for (r in 0 until entry.value.size) {
                row = xlWs.createRow(startRow + r)
                row.rowStyle = wrapStyle
                for (c in 0..5) {
                    row.createCell(c).cellStyle = defaultStyle
                }
            }
            row = xlWs.getRow(startRow)
            val cellNumberRange = CellRangeAddress(startRow, startRow + entry.value.size - 1, 0, 0)
            xlWs.addMergedRegion(cellNumberRange)
            val cellNumber =
                row.createCell(cellNumberRange.firstColumn)
            cellNumber.setCellValue(
                indexInList.toString()
            )
            cellNumber.cellStyle = defaultStyle
            val cellRoomRange = CellRangeAddress(startRow, startRow + entry.value.size - 1, 1, 1)
            xlWs.addMergedRegion(cellRoomRange)
            val cellRoom =
                row.createCell(cellRoomRange.firstColumn)
            cellRoom.setCellValue(
                entry.key
            )


            cellRoom.cellStyle = defaultStyle

            entry.value.forEachIndexed { index, daoRemarkEntity ->

                row = xlWs.getRow(startRow + index)

                val cellType = row.createCell(2)
                cellType.setCellValue(
                    daoRemarkEntity.typeRemark
                )
                cellType.cellStyle = defaultStyle

                val cellDescriptionRange = CellRangeAddress(
                    startRow + index,
                    startRow + index,
                    3,
                    if (layout == R.layout.fragment_export_to_excel) 5 else 4
                )
                xlWs.addMergedRegion(cellDescriptionRange)
                val cellDescription = row.createCell(3)

                var text = daoRemarkEntity.nameRemark
                var array = daoRemarkEntity.comment.split("   ")
                if (array.isNotEmpty()) {
                    array = array.subList(1, array.size)
                }
                array.forEach { text += it }
                cellDescription.setCellValue(
                    text
                )
                cellDescription.cellStyle = defaultStyle

                if (layout != R.layout.fragment_export_to_excel) {
                    val cellComplete = row.createCell(5)
                    cellComplete.setCellValue(if (daoRemarkEntity.isComplete == true) "Да" else "Нет")
                    cellComplete.cellStyle = defaultStyle
                }
            }
            startRow += entry.value.size
            indexInList++
        }

        return startRow
    }

    private fun writeConsumerToExcel(xlWs: Sheet, index: Int) {
        var startIndex = index
        val defaultStyle = xlWs.workbook.createCellStyle()
        defaultStyle.wrapText = true
        defaultStyle.verticalAlignment = 1
        val defaultFont = xlWs.workbook.createFont()
        defaultFont.bold = false
        defaultFont.fontHeightInPoints = 11
        defaultStyle.setFont(defaultFont)
        if (apartmentEntity.fullName.isEmpty()) apartmentEntity.fullName.add("")
        apartmentEntity.fullName.forEachIndexed { indexFullName, it ->
            for (i in 0..2) {
                val row = xlWs.createRow(startIndex + i)
                for (cell in 1..5) {
                    row.createCell(cell).cellStyle = defaultStyle
                }
                row.height = (row.height * 3).toShort()
                val cellType = row.createCell(1)
                cellType.setCellValue(
                    when (i) {
                        0 -> "ФИО"
                        1 -> "Подпись:"
                        else -> "Дата:"
                    }
                )
                cellType.cellStyle = defaultStyle
                if (indexFullName == 0) {
                    val cellBuilderRange = CellRangeAddress(row.rowNum, row.rowNum, 4, 5)
                    xlWs.addMergedRegion(cellBuilderRange)
                    val cellBuilder = row.createCell(4)
                    cellBuilder.setCellValue(
                        when (i) {
                            0 -> "ФИО"
                            1 -> "Подпись:"
                            else -> "Дата:"
                        }
                    )
                    cellBuilder.cellStyle = defaultStyle
                }
                val cellDescriptionRange = CellRangeAddress(startIndex + i, startIndex + i, 2, 3)
                xlWs.addMergedRegion(cellDescriptionRange)
                if (i == 0) {
                    val cellDescription = row.createCell(2)
                    cellDescription.setCellValue(
                        it
                    )
                    cellDescription.cellStyle = defaultStyle
                }
            }
            startIndex += 3
        }
    }

    open fun loadData() = GlobalScope.launch(Dispatchers.Main) {
        withContext(Dispatchers.IO) {
            if (!apartmentEntity.rooms.contains(getString(R.string.koridor))) {
                apartmentEntity.rooms.add(getString(R.string.koridor))
            }
            if (!apartmentEntity.rooms.contains(getString(R.string.balkon))) {
                apartmentEntity.rooms.add(getString(R.string.balkon))
            }
            if (!apartmentEntity.rooms.contains(getString(R.string.parking_item))) {
                apartmentEntity.rooms.add(getString(R.string.parking_item))
            }
            apartmentEntity.rooms.forEach {
                updateFullList(it)
                updateList(it).join()
            }
        }
        withContext(Dispatchers.Main) {
            mainRecycler?.adapter = AdapterForListRemarkExcel(
                map.keys.toList(),
                map
            )
        }
    }

    private fun updateFullList(room: String) = GlobalScope.launch(Dispatchers.Main) {
        withContext(Dispatchers.IO) {
            Repository.getRemarkListFromApartmentFromRoom(apartmentEntity.id!!, room) {
                val resList = it!!.filter { remark -> remark.isShow }
                if (resList.isNotEmpty()) {
                    fullMap[room] = resList
                }
            }
        }
    }


    open fun updateList(room: String) = GlobalScope.launch(Dispatchers.Main) {
        withContext(Dispatchers.IO) {
            Repository.getRemarkListFromApartmentFromRoom(apartmentEntity.id!!, room) {
                val resList = it!!.filter { remark -> remark.isShow }
                if (resList.isNotEmpty()) {
                    map[room] = resList
                }
            }
        }
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == WRITE_CODE && grantResults.isNotEmpty()) {
            val cameraPermissionResult = grantResults[0]
            if (cameraPermissionResult == PackageManager.PERMISSION_GRANTED) {
                sendData()
            } else {
                Toast.makeText(context!!, "Ошибка", Toast.LENGTH_SHORT).show()
            }
        }
    }
}