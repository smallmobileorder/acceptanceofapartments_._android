package com.acceptance.apartments.fragments.other_fragments

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.acceptance.apartments.R
import com.acceptance.apartments.adapters.SwipeToDeleteCallback
import com.acceptance.apartments.adapters.other_adapters.AdapterTypeReview
import com.acceptance.apartments.fragments.AbstractFragment
import com.acceptance.apartments.model.DaoApartmentEntity
import com.acceptance.apartments.model.Repository
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.bottom_sheet_load_prompt.view.*
import kotlinx.android.synthetic.main.component_botton_of_header.*
import kotlinx.android.synthetic.main.component_btn_back.view.*
import kotlinx.android.synthetic.main.component_recycler_view.*
import kotlinx.android.synthetic.main.fragment_room_for_review.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


open class TypeReviewFragment : AbstractFragment(R.layout.fragment_room_for_review) {
    open val isResult = false
    var apartmentList = mutableListOf<DaoApartmentEntity>()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initBottomView()
        initButtonBack()
    }

    override fun onResume() {
        super.onResume()
        loadData()
    }

    open fun loadData() = GlobalScope.launch(Dispatchers.Main) {
        withContext(Dispatchers.IO) {
            apartmentList.clear()
            val apartmentListFull = mutableListOf<DaoApartmentEntity>()
            Repository.getApartmentList {
                apartmentListFull.addAll(it.filter { daoApartmentEntity -> daoApartmentEntity.progressIsComplete && daoApartmentEntity.reviewIsComplete })
            }
            val apartmentEntitySet = mutableSetOf<DaoApartmentEntity>()
            apartmentListFull.forEach { apartment ->
                apartmentEntitySet.add(
                    findLastReview(
                        apartment,
                        apartmentListFull
                    )
                )
            }

            apartmentEntitySet.forEach { apartment ->
                completeAllRemark(apartment.id!!) { result ->
                    if (!result) apartmentList.add(apartment)
                }.join()
            }
        }
        withContext(Dispatchers.Main) {
            updateRecycler()
        }
    }

    private fun findLastReview(
        apartmentEntity: DaoApartmentEntity,
        listApartmentEntity: List<DaoApartmentEntity>
    ): DaoApartmentEntity {
        val lastDaoList =
            listApartmentEntity.filter { dao -> dao.idFirstReview == apartmentEntity.idFirstReview || dao.idFirstReview == apartmentEntity.id }
        return lastDaoList.maxBy { it.id!! } ?: apartmentEntity
    }

    private fun completeAllRemark(id: Long, function: (result: Boolean) -> Unit) =
        GlobalScope.launch(Dispatchers.Main) {
            withContext(Dispatchers.IO) {
                var result = true
                Repository.getRemarkListFromApartment(id) {
                    val resList = it!!.filter { remark -> remark.isShow }
                    resList.forEach { remark ->
                        if (remark.isComplete != true) {
                            result = false
                        }
                    }
                }
                function(result)
            }
        }

    fun updateRecycler() {
        var adapterR: AdapterTypeReview? = null
        GlobalScope.launch(Dispatchers.Main) {
            withContext(Dispatchers.IO) {
                apartmentList.sortBy { it.id }
                apartmentList.reverse()
                val listName = Array(apartmentList.size) { "" }
                adapterR = AdapterTypeReview(
                    listName.toList(),
                    apartmentList,
                    isResult,
                    false
                )
            }
            mainRecycler?.adapter = adapterR
            val swipeHandler = object : SwipeToDeleteCallback(context!!) {
                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                    val adapter = mainRecycler?.adapter as AdapterTypeReview?
                    val position = viewHolder.adapterPosition
                    var apartmentEntity: DaoApartmentEntity? = null
                    val objLayoutParams =
                        LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT
                        )
                    val snackbar = Snackbar.make(mainRecycler, "Подтвердить удаление?", Snackbar.LENGTH_INDEFINITE)
                    snackbar.view.setBackgroundColor(resources.getColor(R.color.invisible))
                    val layout = snackbar.view as Snackbar.SnackbarLayout
                    val parentParams = layout.layoutParams as FrameLayout.LayoutParams
                    layout.layoutParams = parentParams
                    layout.setPadding(0, 0, 0, 0)
                    layout.layoutParams = parentParams
                    // Inflate our custom view
                    val snackView = layoutInflater.inflate(R.layout.bottom_sheet_load_prompt, null)

                    snackView.textViewOne.setOnClickListener {
                        adapter?.removeAt(position) { apartmentEntity = it }
                        Repository.deleteApartmentAsynk(apartmentEntity!!)
                        snackbar.dismiss()
                    }

                    snackView.textViewTwo.setOnClickListener {
                        adapter?.notifyItemChanged(position)
                        snackbar.dismiss()
                    }

                    snackbar.addCallback(object : BaseTransientBottomBar.BaseCallback<Snackbar>() {
                        override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                            super.onDismissed(transientBottomBar, event)
                            if (event != DISMISS_EVENT_MANUAL) {
                                adapter?.notifyItemChanged(position)
                            }
                        }
                    })


                    layout.addView(snackView, objLayoutParams)
                    snackbar.show()
                }
            }
            val itemTouchHelper = ItemTouchHelper(swipeHandler)
            itemTouchHelper.attachToRecyclerView(mainRecycler)
        }
    }


    private fun initButtonBack() {
        btn.btnBack.setOnClickListener { activity.onBackPressed() }
    }

    open fun initBottomView() {
        bottomOfHeader.text = getString(R.string.type_review_fragment_text)
    }

}