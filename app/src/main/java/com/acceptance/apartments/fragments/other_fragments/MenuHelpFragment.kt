package com.acceptance.apartments.fragments.other_fragments

import android.os.Bundle
import android.view.View
import com.acceptance.apartments.R
import com.acceptance.apartments.adapters.other_adapters.AdapterMenu
import com.acceptance.apartments.fragments.AbstractFragment
import kotlinx.android.synthetic.main.component_recycler_view.*

class MenuHelpFragment : AbstractFragment(R.layout.fragment_menu_help) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainRecycler?.adapter = AdapterMenu(resources.getStringArray(R.array.menu_help).toList(), R.array.menu_help)
    }


}