package com.acceptance.apartments

import android.content.Intent
import android.graphics.Point
import android.media.ExifInterface
import android.os.Bundle
import android.text.TextUtils
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.setPadding
import androidx.lifecycle.ViewModelProviders
import com.acceptance.apartments.fragments.AbstractFragment
import com.acceptance.apartments.fragments.other_fragments.*
import com.acceptance.apartments.model.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.component_header.*
import kotlinx.android.synthetic.main.component_header.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.Reader


class MainActivity : AppCompatActivity() {

    private val listFragment = mutableListOf<AbstractFragment>()

    private var lastFragment: AbstractFragment? = null

    lateinit var model: MyViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_main)
        super.onCreate(savedInstanceState)
        model = ViewModelProviders.of(this).get(MyViewModel::class.java)
        model.activity = this
        initFragment(MenuFragment())
        initListData()
    }

    fun initFragment(fragment: AbstractFragment) {
        if (lastFragment != null && lastFragment!!::class.java == fragment::class.java) {
            return
        }
        listFragment.add(fragment)
        viewFragment(fragment)
    }

    private fun viewFragment(fragment: AbstractFragment, isNext: Boolean = true) {
        progressView.visibility = VISIBLE
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        if (isNext) {
            if (fragment::class.java != ListRemarkFragment::class.java)
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
        } else {
            fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
        }
        if (lastFragment != null) {
            lastFragment!!.onPause()
            fragmentTransaction.hide(lastFragment!!)
        }
        if (fragmentManager.fragments.contains(fragment)) {
            fragment.onResume()
            fragmentTransaction.show(fragment)
            progressView.visibility = GONE
        } else {
            fragmentTransaction.add(R.id.fragment_container, fragment)
        }
        lastFragment = fragment
        fragmentTransaction.commit()
        updateIconHead(fragment::class.java)

        if (model.newApartmentEntity.id != null)
            GlobalScope.launch(Dispatchers.Main) {
                withContext(Dispatchers.IO) {
                    Repository.updateApartment(model.newApartmentEntity)
                }
            }
    }

    private fun updateIconHead(classJava: Class<out AbstractFragment>) {
        val display = windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        val width = size.x
        val padding = (40.0 / 1080 * width).toInt()
        val paddingLittle = (20.0 / 1080 * width).toInt()
        when (classJava) {
            NewRemarkFragment::class.java, ListRemarkFragment::class.java -> {
                head?.let { head.visibility = VISIBLE }
                head.iconLayout.setPadding(0)
                head.iconLayout.setImageDrawable(model.backGroundFragment.first)
                checkAllBtn.visibility = if (classJava == ListRemarkFragment::class.java) VISIBLE else GONE
            }
            TypeWorkingFragment::class.java -> {
                main_background.setBackgroundColor(ContextCompat.getColor(this, R.color.invisible))
                head.iconLayout.setPadding(0)
                head?.let { head.visibility = GONE }
                checkAllBtn.visibility = GONE
                headerLine.setBackgroundColor(resources.getColor(R.color.colorPrimaryDark))
            }
            HelpFragment::class.java -> {
                head.iconLayout.setPadding(padding)
                headerLine.setBackgroundColor(resources.getColor(R.color.colorPrimaryDark))
            }
            MenuFragment::class.java -> {
                head.iconLayout.setPadding(paddingLittle)
                head.iconLayout.setImageDrawable(resources.getDrawable(R.drawable.user_invisible))
                headerLine.setBackgroundColor(resources.getColor(R.color.colorPrimaryDark))

            }
            MenuHelpFragment::class.java -> {
                head.iconLayout.setPadding(padding)
                head.iconLayout.setImageDrawable(resources.getDrawable(R.drawable.literature_filled))
                headerLine.setBackgroundColor(resources.getColor(R.color.colorPrimaryDark))

            }
            else -> {
                when (classJava) {
                    SourceApartmentFragment::class.java, TypeRemontFragment::class.java, EnterDataFragment::class.java -> {
                        head.iconLayout.setImageDrawable(resources.getDrawable(R.drawable.settings_invisible))
                        head.iconLayout.setPadding(padding)
                    }
                    else -> {
                        head.iconLayout.setPadding(paddingLittle)
                        head.iconLayout.setImageDrawable(resources.getDrawable(R.drawable.user_invisible))
                    }
                }
                headerLine.setBackgroundColor(resources.getColor(R.color.colorPrimaryDark))
                checkAllBtn.visibility = GONE
                head?.let { head.visibility = VISIBLE }
                main_background.setBackgroundColor(ContextCompat.getColor(this, R.color.invisible))
            }

        }
    }

    override fun onBackPressed() {
        if (listFragment.size > 1) {
            supportFragmentManager.beginTransaction().remove(listFragment.last()).commit()
            listFragment.remove(listFragment.last())
            viewFragment(listFragment.last(), false)
        }
    }

    fun clearFragments(){
        while (listFragment.size > 1) {
            supportFragmentManager.beginTransaction().remove(listFragment.last()).commit()
            listFragment.remove(listFragment.last())
        }
        viewFragment(listFragment.last(), false)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        this.lastFragment?.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        this.lastFragment?.onActivityResult(requestCode, resultCode, data)
    }


    /**json bloc start*/
    private fun initListData() {
        model.listRemarkFromJson = fromJson(resources.openRawResource(R.raw.appartment).bufferedReader())
        model.listRemarkFromJsonCommerce = fromJson(resources.openRawResource(R.raw.commercy).bufferedReader())
        model.listRemarkFromJsonParking = fromJson(resources.openRawResource(R.raw.parking).bufferedReader())

    }

    private fun fromJson(value: Reader): MutableMap<String, MutableList<RemarkGroup>> {
        val mapType = object : TypeToken<MutableMap<String, MutableList<RemarkGroup>>>() {}.type
        return Gson().fromJson<MutableMap<String, MutableList<RemarkGroup>>>(value, mapType)
    }

    companion object {
        fun fromJsonSection(value: Reader): MutableList<SectionHelp> {
            val mapType = object : TypeToken<MutableList<SectionHelp>>() {}.type
            return Gson().fromJson<MutableList<SectionHelp>>(value, mapType)
        }

        fun getExif(filePath: String): ExifInterface? {
            try {
                return ExifInterface(filePath)
            } catch (e: IOException) {
                e.printStackTrace()
            }

            return null
        }

        fun setExif(filePath: String, exif: ExifInterface) {
            try {
                val newExif = ExifInterface(filePath)
                val cls = ExifInterface::class.java
                val fields = cls.fields
                for (field in fields) {
                    val fieldName = field.name
                    if (!TextUtils.isEmpty(fieldName) && fieldName.startsWith("TAG")) {
                        val tag = field.get(cls).toString()
                        val value = exif.getAttribute(tag)
                        if (value != null) {
                            newExif.setAttribute(tag, value)
                        }
                    }
                }
                newExif.saveAttributes()
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }



    }

    private fun writeToStorage(content: String) {
        val myExternalFile = File(getExternalFilesDir("apartment"), "json4.json")
        try {
            val fileOutPutStream = FileOutputStream(myExternalFile)
            fileOutPutStream.write(content.toByteArray())
            fileOutPutStream.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    private fun toJson(value: MutableMap<String, MutableList<RemarkGroup>>?): String? {
        return Gson().toJson(value)
    }

    private fun testSplit(id: Int): MutableList<RemarkGroup> {
        val inputStream = resources.openRawResource(id)
        val listGroupRemark = mutableListOf<RemarkGroup>()
        val listNameSubGroup = mutableListOf<String>()
        var linesList = mutableListOf<String>()
        inputStream.bufferedReader().useLines { lines ->
            linesList = lines.toMutableList()
        }
        var remarkGroup = RemarkGroup()
        var remark = Remark()
        val firstLine = linesList.first()
        var lastSubRemark = SubRemark()
        linesList.forEach { line ->
            println(line)
            if (line == firstLine) {
                line.split("%%").forEach { subName ->
                    if (subName.isNotEmpty()) listNameSubGroup.add(subName)
                }
            } else {
                val parts = line.split("%%")

                if (parts[0].isNotEmpty()) {
                    if (remarkGroup != RemarkGroup()) {
                        listGroupRemark.add(remarkGroup)
                    }
                    remarkGroup = RemarkGroup(parts[0], mutableListOf())
                }

                if (parts[1].isNotEmpty()) {
                    if (remark != Remark()) {
                        remarkGroup.listRemark.add(remark)
                    }
                    remark = Remark(parts[1], parts[2], mutableMapOf())
                }

                if (parts[3].isNotEmpty() && parts[4].isNotEmpty()) {
                    lastSubRemark = SubRemark(levelCritic = parts[3].toInt(), actionIfExist = parts[4])
                }

                val startIndex = 5
                var isContainsElement = false
                for (value in startIndex..startIndex - 1 + listNameSubGroup.size) {
                    if (!remark.mapPlaceRemark.contains(listNameSubGroup[value - startIndex])) remark.mapPlaceRemark[listNameSubGroup[value - startIndex]] =
                        mutableListOf()
                    if (parts[value].isNotEmpty()) {
                        remark.mapPlaceRemark[listNameSubGroup[value - startIndex]]!!.add(lastSubRemark.copy(remark = parts[value]))
                        isContainsElement = true
                    }
                }
                if (!isContainsElement) {
                    remark.mapPlaceRemark.clear()
                    remark.currentSubRemark = lastSubRemark
                }
            }
        }
        remarkGroup.listRemark.add(remark)
        listGroupRemark.add(remarkGroup)
        return listGroupRemark
    }

    /**json bloc end*/

}
