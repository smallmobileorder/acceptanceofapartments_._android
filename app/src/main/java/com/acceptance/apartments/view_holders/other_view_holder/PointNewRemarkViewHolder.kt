package com.acceptance.apartments.view_holders.other_view_holder

import android.view.View
import com.acceptance.apartments.MainActivity
import com.acceptance.apartments.R
import com.acceptance.apartments.adapters.other_adapters.GridAdapterForNewRemark
import com.acceptance.apartments.model.SubRemark
import com.acceptance.apartments.utils.common.toColorStateList
import com.acceptance.apartments.view_holders.AbstractViewHolder
import kotlinx.android.synthetic.main.item_new_remark_grid_point.view.*


class PointNewRemarkViewHolder(val view: View) : AbstractViewHolder(view) {
    var subRemark: SubRemark? = null
    override fun bind(vararg params: Any) {
        subRemark = params[0] as SubRemark
        val adapter = params[1] as GridAdapterForNewRemark
        view.crp_is_valid_check_box.setOnClickListener {
            adapter.setIsSelected(view.crp_description_text_view.text.toString())
        }
        val activity = view.context as MainActivity
        var color = activity.model.backGroundFragment.second
        view.crp_is_valid_check_box.buttonTintList = color.toColorStateList()
        if (color == view.resources.getColor(R.color.colorVitragFon)) {
            color = view.resources.getColor(R.color.colorVitrag)
        }
        view.crp_description_text_view.setTextColor(color)
        view.crp_description_text_view.text = subRemark!!.remark
        adapter.listView.add(this)
    }
}