package com.acceptance.apartments.view_holders.other_view_holder

import android.view.View
import com.acceptance.apartments.adapters.other_adapters.AdapterForSectionRemarkExcel
import com.acceptance.apartments.model.DaoRemarkEntity
import com.acceptance.apartments.view_holders.AbstractViewHolder
import kotlinx.android.synthetic.main.item_remark_section.view.*


class ListRemarkExcelViewHolder(private val view: View) : AbstractViewHolder(view) {
    override fun bind(vararg params: Any) {

        val name = params[0] as String
        val list = params[1] as List<DaoRemarkEntity>
        view.crs_header_text_view.text = name
        val listName = mutableListOf<String>()
        list.forEach { listName.add(it.nameRemark) }
        view.crs_recycler_view.adapter = AdapterForSectionRemarkExcel(listName, list)
    }
}