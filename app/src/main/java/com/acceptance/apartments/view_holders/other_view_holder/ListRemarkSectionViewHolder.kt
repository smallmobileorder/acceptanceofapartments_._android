package com.acceptance.apartments.view_holders.other_view_holder

import android.annotation.SuppressLint
import android.graphics.Paint
import android.text.Html
import android.view.View
import android.widget.RelativeLayout
import androidx.core.view.marginEnd
import com.acceptance.apartments.MainActivity
import com.acceptance.apartments.R
import com.acceptance.apartments.adapters.other_adapters.AdapterForListRemark
import com.acceptance.apartments.fragments.other_fragments.NewRemarkFragment
import com.acceptance.apartments.model.DaoRemarkEntity
import com.acceptance.apartments.model.Remark
import com.acceptance.apartments.utils.common.setAlpha
import com.acceptance.apartments.utils.common.toColorStateList
import com.acceptance.apartments.view_holders.AbstractViewHolder
import kotlinx.android.synthetic.main.component_divider_bold_invisible.view.*
import kotlinx.android.synthetic.main.component_divider_invisible.view.*
import kotlinx.android.synthetic.main.item_remark_point.view.*
import kotlin.concurrent.thread

class ListRemarkSectionViewHolder(
    private val view: View,
    var adapterForListRemark: AdapterForListRemark
) : AbstractViewHolder(view) {

    private var color: Int = 0
    private lateinit var listAllRemark: List<DaoRemarkEntity>
    private lateinit var remark: Remark
    val activity = view.context as MainActivity

    @SuppressLint("SetTextI18n")
    override fun bind(vararg params: Any) {
        remark = params[0] as Remark
        val type = params[1] as String
        listAllRemark = params[2] as List<DaoRemarkEntity>
        view.crp_description_text_view.text = remark.remark
        view.crp_tip_text_view.text = Html.fromHtml(remark.comment)
        remark.docs.forEach {
            if (it.isNotEmpty())
                view.crp_tip_text_view.text = view.crp_tip_text_view.text.toString() + " ($it)"
        }
        color = activity.model.backGroundFragment.second
        val invisibleColor = color.setAlpha(0.4f)
        view.dividerViewCard.setCardBackgroundColor(invisibleColor)
        view.dividerSlim.setBackgroundColor(invisibleColor)
        view.crp_add_clarification_button.iconTint = invisibleColor.toColorStateList()
        view.crp_is_valid_check_box.buttonTintList = color.toColorStateList()
        if (color == view.resources.getColor(R.color.colorVitragFon)) {
            color = view.resources.getColor(R.color.colorVitrag)
        }
        view.crp_description_text_view.setTextColor(color)
        view.crp_tip_text_view.setTextColor(color)
        view.crp_description_text_view.setOnClickListener {
            if (view.crp_tip_text_view.visibility == View.VISIBLE) {
                view.crp_tip_text_view.visibility = View.GONE
            } else {
                view.crp_tip_text_view.visibility = View.VISIBLE
            }
            toggle()
        }
        if (activity.model.newApartmentEntity.progressReview.find { remark.remark == it.remark } != null) {
            view.crp_is_valid_check_box.isChecked = true
            view.crp_description_text_view.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
        }
        if (listAllRemark.find { it.nameRemark == remark.remark } != null) {
            view.crp_is_valid_check_box.isChecked = false
            view.crp_is_valid_check_box.isEnabled = false
            view.crp_add_clarification_button.iconTint = color.toColorStateList()
            view.crp_is_valid_check_box.buttonDrawable =
                view.resources.getDrawable(R.drawable.ic_indeterminate_check_box_black_24dp)
            view.crp_is_valid_check_box.post {
                val paramsMargin = view.crp_is_valid_check_box.layoutParams as RelativeLayout.LayoutParams
                paramsMargin.marginEnd = (view.crp_is_valid_check_box.marginEnd * 0.75).toInt()
                view.crp_is_valid_check_box.layoutParams = paramsMargin
            }
            view.crp_description_text_view.paintFlags = Paint.LINEAR_TEXT_FLAG
            thread {
                Thread.sleep(100)
                activity.model.newApartmentEntity.progressReview.add(remark)
            }
        }

        view.crp_is_valid_check_box.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                activity.model.newApartmentEntity.progressReview.add(remark)
                view.crp_description_text_view.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
            } else {
                activity.model.newApartmentEntity.progressReview.remove(remark)
                view.crp_description_text_view.paintFlags = Paint.LINEAR_TEXT_FLAG
            }
        }

        view.crp_add_clarification_button.setOnClickListener {
            activity.model.isCustomRemark = false
            activity.initFragment(NewRemarkFragment(remark, type))
        }
        adapterForListRemark.mutableListItem.add(this)
    }

    fun notifyCheck() {
        if (activity.model.newApartmentEntity.progressReview.find { remark.remark == it.remark } != null) {
            view.crp_is_valid_check_box.isChecked = true
            view.crp_description_text_view.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
        } else {
            view.crp_is_valid_check_box.isChecked = false
            view.crp_description_text_view.paintFlags = Paint.LINEAR_TEXT_FLAG
        }
        if (listAllRemark.find { it.nameRemark == remark.remark } != null) {
            view.crp_is_valid_check_box.isChecked = false
            view.crp_is_valid_check_box.isEnabled = false
            view.crp_add_clarification_button.iconTint = color.toColorStateList()
            view.crp_is_valid_check_box.buttonDrawable =
                view.resources.getDrawable(R.drawable.ic_indeterminate_check_box_black_24dp)
            view.crp_is_valid_check_box.post {
                val paramsMargin = view.crp_is_valid_check_box.layoutParams as RelativeLayout.LayoutParams
                paramsMargin.marginEnd = (view.crp_is_valid_check_box.marginEnd * 0.75).toInt()
                view.crp_is_valid_check_box.layoutParams = paramsMargin
            }
            view.crp_description_text_view.paintFlags = Paint.LINEAR_TEXT_FLAG
            thread {
                Thread.sleep(100)
                activity.model.newApartmentEntity.progressReview.add(remark)
            }
        }
    }

    private fun toggle() {
        view.dividerView.visibility = view.crp_tip_text_view.visibility
        view.toggle_view.visibility =
            if (view.dividerView.visibility == View.GONE) View.VISIBLE else View.GONE
    }
}