package com.acceptance.apartments.view_holders.other_view_holder

import android.view.View
import com.acceptance.apartments.MainActivity
import com.acceptance.apartments.R
import com.acceptance.apartments.fragments.other_fragments.ListRemarkFragment
import com.acceptance.apartments.model.Remark
import com.acceptance.apartments.model.RemarkGroup
import com.acceptance.apartments.utils.common.toColorStateList
import kotlinx.android.synthetic.main.item_type_work.view.*

class TypeWorkViewHolder(private val view: View) {
    private var iconInvisible = ""
    val mainActivity = (view.context as MainActivity)

    var icon = ""

    lateinit var remarkList: MutableList<RemarkGroup>
    private var isEnable = false

    fun bind(vararg list: Any) {
        val text = list[0] as String
        isEnable = list[1] as Boolean
        val isChecked = list[2] as Boolean
        var background = list[3] as String
        icon = list[4] as String
        remarkList = list[5] as MutableList<RemarkGroup>
        iconInvisible = list[6] as String
        background = background.split("/")[2].split(".")[0]
        icon = icon.split("/")[2].split(".")[0]
        iconInvisible = iconInvisible.split("/")[2].split(".")[0]
        if (iconInvisible == "fasad_invisible") {
            iconInvisible = "balcony_invisible"
        }
        if (iconInvisible == "vitrag_invisible") {
            iconInvisible = "vitrag_invisible2"
        }
        view.btn.text = text

        if (isEnable) {
            view.view_background.background =
                view.resources.getDrawable(
                    view.resources.getIdentifier(
                        background,
                        "drawable",
                        view.context.packageName
                    )
                )
            view.failedIcon.visibility = View.GONE
            view.btn.setOnClickListener {
                clickListener(text)
            }
            view.icon.setOnClickListener {
                clickListener(text)
            }
            view.view_background.setOnClickListener {
                clickListener(text)
            }
        } else {
            view.btn.setTextColor(view.resources.getColor(R.color.colorFailed))
            view.failedIcon.imageTintList = view.resources.getColor(R.color.colorFailed).toColorStateList()
            view.btn.setOnClickListener {}
            view.icon.setOnClickListener {}
            view.view_background.setOnClickListener {}
        }

    }


    private fun clickListener(text: String) {
        val array = view.resources.getStringArray(R.array.workType)
        array.forEachIndexed { index, str ->
            if (str == text) {
                val color = view.resources.getIntArray(R.array.backgroundColorList)[index]
                mainActivity.model.backGroundFragment =
                    Pair(
                        view.resources.getDrawable(
                            view.resources.getIdentifier(
                                icon,
                                "drawable",
                                view.context.packageName
                            )
                        ),
                        color
                    )
            }
        }
        mainActivity.initFragment(ListRemarkFragment(remarkList, text))
    }

    fun notifyItemChanged() {
        val listRemark = mutableListOf<Remark>()
        remarkList.forEach { group ->
            group.listRemark.forEach { remark -> listRemark.add(remark) }
        }
        var flag = true
        listRemark.forEach {
            val list = mainActivity.model.newApartmentEntity.progressReview.toList()
            if (!list.contains(it)) {
                flag = false
                return@forEach
            }
        }
        view.icon.setImageDrawable(
            view.resources.getDrawable(
                if (flag) R.drawable.complete else view.resources.getIdentifier(
                    if (isEnable) icon else iconInvisible,
                    "drawable",
                    view.context.packageName
                )
            )
        )

    }
}