package com.acceptance.apartments.view_holders.other_view_holder

import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.acceptance.apartments.MainActivity
import com.acceptance.apartments.adapters.other_adapters.GridAdapterForNewRemark
import com.acceptance.apartments.model.SubRemark
import com.acceptance.apartments.view_holders.AbstractViewHolder
import kotlinx.android.synthetic.main.item_new_remark_section.view.*


class ListNewRemarkViewHolder(private val view: View) : AbstractViewHolder(view) {
    override fun bind(vararg params: Any) {
        val text = params[0] as String
        val list = params[1] as List<SubRemark>
        view.crs_header_text_view.text = text

        val recycleLayoutManager = GridLayoutManager(view.context, 2, RecyclerView.VERTICAL, false)
        recycleLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                // grid items to take 1 column
                return if (list.size % 2 == 0) {
                    1
                } else {
                    if (position == list.size - 1) 2 else 1
                }
            }
        }
        view.gridView.layoutManager = recycleLayoutManager
        view.gridView.adapter = GridAdapterForNewRemark(list)
        val activity = view.context as MainActivity
        val color = activity.model.backGroundFragment.second
        view.card_recycler_view.setCardBackgroundColor(color)
        view.crs_header_text_view.setTextColor(color)
    }

    fun getCurrentSubRemark() = (view.gridView.adapter as GridAdapterForNewRemark).pointNewRemarkViewHolder

}