package com.acceptance.apartments.view_holders

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class AbstractViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    abstract fun bind(vararg params: Any)

}
