package com.acceptance.apartments.view_holders.other_view_holder

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.text.Html
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.PopupWindow
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.get
import com.acceptance.apartments.MainActivity
import com.acceptance.apartments.R
import com.acceptance.apartments.model.SectionHelp
import com.acceptance.apartments.view_holders.AbstractViewHolder
import kotlinx.android.synthetic.main.component_help_section.view.*
import kotlinx.android.synthetic.main.component_help_section_liner.view.*
import kotlinx.android.synthetic.main.component_help_section_text.view.*
import kotlinx.android.synthetic.main.component_pop_up_photo.view.*
import kotlinx.android.synthetic.main.item_help_section.view.*


class HelpViewHolder(
    private val view: View,
    private val idList: Int
) : AbstractViewHolder(view) {
    override fun bind(vararg params: Any) {
        val sectionHelp = params[0] as SectionHelp
        val activity = view.context as MainActivity
        if (!sectionHelp.section.isNullOrEmpty()) {
            view.headerText.visibility = View.VISIBLE
            view.headerText.text = sectionHelp.section
            view.headerText.post {
                if (sectionHelp.section == "Также, к критичным замечаниям следует отнести:") {
                    val layoutParams = view.headerText.layoutParams as ConstraintLayout.LayoutParams
                    layoutParams.marginStart = (layoutParams.marginStart / 2.0).toInt()
                    view.headerText.layoutParams = layoutParams
                }
            }
            if (idList == R.raw.help) {
//                view.headerText.setOnClickListener {
//                    val position = when (view.headerText.text.toString()) {
//                        "Подготовка к приёмке" -> 0
//                        "Первичный осмотр" -> 1
//                        "Повторный осмотр" -> 2
//                        "Мои осмотры" -> 3
//                        else -> 0
//                    }
//                    activity.initFragment(HelpFragment(R.raw.help, position))
//                }

                view.headerText.setOnClickListener {
                    if (!sectionHelp.items.isNullOrEmpty()) {
                        view.sectionLayout.removeAllViews()
                        sectionHelp.items!!.forEach {
                            if (!it.text.isNullOrEmpty()) {
                                val text = View.inflate(view.context, R.layout.component_help_section_text, null)
                                if (it.text == "<b><i>Если в квартире более одной жилой комнаты – их нумерация начинается от входной двери, и далее считается по часовой стрелке.</i></b>") {
                                    text.textItem.background = null
                                    text.textItem.post {
                                        text.textItem.setTextSize(TypedValue.COMPLEX_UNIT_SP, 11f)
                                        val layoutParams = text.textItem.layoutParams as LinearLayout.LayoutParams
                                        layoutParams.topMargin = 0
                                        text.textItem.layoutParams = layoutParams
                                    }
                                }
                                text.textItem.text =
                                    Html.fromHtml(it.text!!.replace("   ".toRegex(), "&nbsp;&nbsp;&nbsp;"))
                                view.sectionLayout.addView(text)
                            }
                            if (it.images != null && !it.images!!.pairs.isNullOrEmpty()) {
                                val liner = View.inflate(view.context, R.layout.component_help_section_liner, null)
                                view.sectionLayout.addView(liner)
                                it.images!!.pairs.forEach { pairs ->
                                    val block = View.inflate(view.context, R.layout.component_help_section, null)
                                    block.textBlock.text = pairs.title
                                    block.imageBlock.setImageDrawable(
                                        view.resources.getDrawable(
                                            view.resources.getIdentifier(
                                                pairs.named,
                                                "drawable",
                                                view.context.packageName
                                            )
                                        )
                                    )
                                    liner.linerItem.addView(block)
                                }
                                view.sectionLayout.post {
                                    val listSize = it.images!!.pairs.size
                                    it.images!!.pairs.forEachIndexed { index, pairs ->
                                        val item = liner.linerItem[index]
                                        item.post {
                                            val widthItem = view.display.width * 1.0 / listSize

                                            val paramsLayout = item.layoutParams
                                            var scaleValue = item.width / widthItem
                                            if (scaleValue > 0) {
                                                if (listSize == 1) {
                                                    scaleValue *= 2
                                                }
                                                if (listSize == 2) {
                                                    scaleValue = (scaleValue * 1.03)
                                                }
                                                paramsLayout.width = (item.width / scaleValue).toInt()
                                                paramsLayout.height = (item.height / scaleValue).toInt()
                                                item.layoutParams = paramsLayout
                                            }
                                        }
                                        item.setOnClickListener {
                                            val inflater =
                                                activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater?
                                            val popupView = inflater!!.inflate(R.layout.component_pop_up_photo, null)
                                            val width = LinearLayout.LayoutParams.MATCH_PARENT
                                            val height = LinearLayout.LayoutParams.MATCH_PARENT
                                            val options = BitmapFactory.Options()
                                            options.inPreferredConfig = Bitmap.Config.ARGB_8888
                                            popupView.photoReprimand.setImageDrawable(
                                                view.resources.getDrawable(
                                                    view.resources.getIdentifier(
                                                        pairs.named,
                                                        "drawable",
                                                        view.context.packageName
                                                    )
                                                )
                                            )
                                            val focusable = true
                                            val popupWindow = PopupWindow(popupView, width, height, focusable)
                                            popupView.setOnClickListener {
                                                popupWindow.dismiss()
                                            }
                                            popupWindow.elevation = 20f
                                            popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0)
                                        }
                                    }
                                }
                            }
                        }

                    } else view.sectionLayout.visibility = View.GONE

                    view.sectionLayout.visibility = if (view.sectionLayout.visibility == View.GONE)
                        View.VISIBLE else View.GONE
                }
                view.sectionLayout.visibility = View.GONE
            }
        } else view.headerText.visibility = View.GONE
        if (idList != R.raw.help) {

            if (!sectionHelp.items.isNullOrEmpty()) {
                view.sectionLayout.removeAllViews()
                sectionHelp.items!!.forEach {
                    if (!it.text.isNullOrEmpty()) {
                        val text = View.inflate(view.context, R.layout.component_help_section_text, null)
                        text.textItem.text = Html.fromHtml(it.text!!.replace("   ".toRegex(), "&nbsp;&nbsp;&nbsp;"))
                        view.sectionLayout.addView(text)
                    }
                    if (it.images != null && !it.images!!.pairs.isNullOrEmpty()) {
                        val liner = View.inflate(view.context, R.layout.component_help_section_liner, null)
                        view.sectionLayout.addView(liner)
                        it.images!!.pairs.forEach { pairs ->
                            val block = View.inflate(view.context, R.layout.component_help_section, null)
                            block.textBlock.text = pairs.title
                            block.imageBlock.setImageDrawable(
                                view.resources.getDrawable(
                                    view.resources.getIdentifier(
                                        pairs.named,
                                        "drawable",
                                        view.context.packageName
                                    )
                                )
                            )
                            liner.linerItem.addView(block)
                        }
                        view.sectionLayout.post {
                            val listSize = it.images!!.pairs.size
                            it.images!!.pairs.forEachIndexed { index, pairs ->
                                val item = liner.linerItem[index]
                                item.post {
                                    val widthItem = view.display.width / listSize
                                    val paramsLayout = item.layoutParams
                                    var scaleValue = item.width / widthItem
                                    if (listSize == 1) {
                                        scaleValue *= 2
                                    }
                                    paramsLayout.width = item.width / scaleValue
                                    paramsLayout.height = item.height / scaleValue
                                    item.layoutParams = paramsLayout
                                }
                            }
                        }
                    }
                }

            } else view.sectionLayout.visibility = View.GONE
        }
    }
}