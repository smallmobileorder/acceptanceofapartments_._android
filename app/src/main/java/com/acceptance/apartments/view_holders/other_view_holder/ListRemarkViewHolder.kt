package com.acceptance.apartments.view_holders.other_view_holder

import android.view.View
import com.acceptance.apartments.MainActivity
import com.acceptance.apartments.adapters.other_adapters.AdapterForListRemark
import com.acceptance.apartments.adapters.other_adapters.AdapterForSectionRemark
import com.acceptance.apartments.model.DaoRemarkEntity
import com.acceptance.apartments.model.RemarkGroup
import com.acceptance.apartments.view_holders.AbstractViewHolder
import kotlinx.android.synthetic.main.item_remark_section.view.*


class ListRemarkViewHolder(
    private val view: View,
    private var listDao: MutableList<DaoRemarkEntity>,
    val adapterForListRemark: AdapterForListRemark
) : AbstractViewHolder(view) {
    override fun bind(vararg params: Any) {
        val group = params[0] as RemarkGroup
        val type = params[1] as String
        view.crs_header_text_view.text = group.group
        val listName = mutableListOf<String>()
        group.listRemark.forEach { listName.add(it.remark) }
        view.crs_recycler_view.adapter = AdapterForSectionRemark(listName, group, type, listDao, adapterForListRemark)
        val activity = view.context as MainActivity
        val color = activity.model.backGroundFragment.second
        view.card_recycler_view.setCardBackgroundColor(color)
        view.crs_header_text_view.setTextColor(color)
    }
}