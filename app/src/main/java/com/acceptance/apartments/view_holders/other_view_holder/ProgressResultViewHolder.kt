package com.acceptance.apartments.view_holders.other_view_holder

import android.view.View
import com.acceptance.apartments.R
import com.acceptance.apartments.utils.common.toColorStateList
import com.acceptance.apartments.view_holders.AbstractViewHolder
import com.acceptance.apartments.view_holders.other_view_holder.ListReprimandViewHolder.Companion.getIndexInResource
import kotlinx.android.synthetic.main.item_progress_result_review.view.*


class ProgressResultViewHolder(private val view: View) : AbstractViewHolder(view) {
    override fun bind(vararg params: Any) {
        val remarkEntity = params[0] as Map.Entry<String, Int>
        val max = params[1] as Int
        val index = getIndexInResource(
            remarkEntity.key,
            view.resources.getStringArray(R.array.workType)
        )
        val colorName = view.resources.getIntArray(R.array.backgroundColorList)[index]

        view.card_count_layout.setCardBackgroundColor(colorName)
        view.card_layout.strokeColor = colorName
        view.nameItem.text = remarkEntity.key.subSequence(0, 2)
        view.nameItem.setTextColor(colorName)
        view.countItem.text = remarkEntity.value.toString()
        view.progress_horizontal.progress = (remarkEntity.value.toDouble() * 100 / max).toInt()
        view.progress_horizontal.progressTintList = colorName.toColorStateList()
    }
}