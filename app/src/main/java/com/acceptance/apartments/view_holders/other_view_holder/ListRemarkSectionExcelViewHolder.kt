package com.acceptance.apartments.view_holders.other_view_holder

import android.annotation.SuppressLint
import android.text.Html
import android.view.View
import com.acceptance.apartments.MainActivity
import com.acceptance.apartments.model.DaoRemarkEntity
import com.acceptance.apartments.view_holders.AbstractViewHolder
import kotlinx.android.synthetic.main.item_remark_point_excel.view.*


class ListRemarkSectionExcelViewHolder(private val view: View) : AbstractViewHolder(view) {
    val activity = view.context as MainActivity

    @SuppressLint("SetTextI18n")
    override fun bind(vararg params: Any) {
        val remark = params[0] as DaoRemarkEntity
        view.typeRemark.text = remark.typeRemark + ":"
        var text = remark.nameRemark
        var array = remark.comment.split("   ")
        if (array.isNotEmpty()) {
            array = array.subList(1, array.size)
        }
        array.forEach { text += it }
        view.typeRemark.text = Html.fromHtml("<u>" + view.typeRemark.text.toString() + "</u> " + text)
    }
}