package com.acceptance.apartments.view_holders.other_view_holder

import android.view.View
import com.acceptance.apartments.MainActivity
import com.acceptance.apartments.R
import com.acceptance.apartments.adapters.other_adapters.AdapterTypeRemont.Companion.arrayUnClear
import com.acceptance.apartments.fragments.other_fragments.ListRemarkFragment
import com.acceptance.apartments.fragments.other_fragments.TypeWorkingFragment
import com.acceptance.apartments.model.RemarkGroup
import com.acceptance.apartments.model.TypeApartment
import com.acceptance.apartments.model.TypeReview
import com.acceptance.apartments.view_holders.AbstractViewHolder
import kotlinx.android.synthetic.main.item_type_remont.view.*

class TypeRemontViewHolder(
    private val view: View
) : AbstractViewHolder(view) {
    override fun bind(vararg params: Any) {
        val text = params[0] as String
        val nameForMap = params[1] as ArrayList<String>
        val map = params[2] as MutableMap<String, MutableList<RemarkGroup>>
        view.itemText.text = text
        view.itemText.post {
            val paramsLayout = view.dividerView.layoutParams
            paramsLayout.width = view.itemText.width
            view.dividerView.layoutParams = paramsLayout
        }
        val activity = (view.context as MainActivity)


        view.itemText.setOnClickListener {
            if (nameForMap.isEmpty()) (view.context as MainActivity).onBackPressed() else {
                activity.model.newApartmentEntity.typeReview = when (text) {
                    "Чистовая\n(Отделка \"Под ключ\")" -> TypeReview.Clear
                    "Предчистовая\n(Подготовка под чистовую)" -> TypeReview.Preclear
                    else -> TypeReview.Unclear
                }
                val currentList = mutableMapOf<String, MutableList<RemarkGroup>>()
                nameForMap.forEach {
                    if (map.contains(it)) {
                        currentList[it] = map[it]!!
                    } else {
                        if (activity.model.listRemarkFromJson.contains(it)) {
                            currentList[it] = activity.model.listRemarkFromJson[it]!!
                        }
                    }
                }
                var count = 0
                currentList.forEach { entry ->
                    if (!((entry.key == "Парковочное место") ||
                                ((!activity.model.newApartmentEntity.rooms.contains(activity.getString(R.string.balkon))) &&
                                        (entry.key == "Фасад" ||
                                                entry.key == "Витражи"))
                                )
                    )
                        entry.value.forEach { group -> count += group.listRemark.size }
                }
                activity.model.newApartmentEntity.progressReviewCount = count
                activity.model.newApartmentEntity.progressReviewAll = currentList
                val nextFragment =
                    if (activity.model.newApartmentEntity.typeApartment != TypeApartment.Parking) {
                        TypeWorkingFragment(currentList)
                    } else {
                        ListRemarkFragment(
                            activity.model.listRemarkFromJsonParking[arrayUnClear.last()]!!,
                            "Парковочное место"
                        )
                    }
                activity.initFragment(nextFragment)
            }
        }
        if (activity.model.newApartmentEntity.typeApartment == TypeApartment.Parking) view.itemText.callOnClick()
    }


}