package com.acceptance.apartments.view_holders.other_view_holder

import android.annotation.SuppressLint
import android.view.View
import androidx.core.content.ContextCompat
import com.acceptance.apartments.MainActivity
import com.acceptance.apartments.R
import com.acceptance.apartments.adapters.other_adapters.AdapterTypeRemont
import com.acceptance.apartments.fragments.other_fragments.*
import com.acceptance.apartments.model.DaoApartmentEntity
import com.acceptance.apartments.model.Repository
import com.acceptance.apartments.model.TypeApartment
import com.acceptance.apartments.view_holders.AbstractViewHolder
import kotlinx.android.synthetic.main.item_room_for_review.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TypeReviewViewHolder(private val view: View, private val isResult: Boolean = false) : AbstractViewHolder(view) {
    @SuppressLint("SetTextI18n")
    override fun bind(vararg params: Any) {
        val apartmentEntity = params[0] as DaoApartmentEntity
        val activity = (view.context as MainActivity)
        view.titleItem?.text = when (apartmentEntity.typeApartment) {
            TypeApartment.Apartment -> "Квартира"
            TypeApartment.Organization -> "Нежилое помещение"
            else -> "Парковочное место"
        }
        if (apartmentEntity.numberRoom.isNotEmpty()) view.titleItem?.text =
            view.titleItem?.text.toString() + " №" + apartmentEntity.numberRoom
        if (apartmentEntity.description.isEmpty()) view.descriptionItem?.visibility = View.GONE
        view.descriptionItem?.text = apartmentEntity.description
        if (isResult) {
            if (apartmentEntity.reviews.size > 1) {
                view.dataItem?.text = "Повторный осмотр от " + apartmentEntity.reviews.last()
            } else {
                view.dataItem?.text = "Первичный осмотр от " + apartmentEntity.reviews.last()
            }
        } else {
            view.dataItem.text = "Осмотр от " + apartmentEntity.reviews.last()
        }
        view.setOnClickListener {
            if (apartmentEntity.progressIsComplete)
                if (!isResult) {
                    activity.initFragment(ListReprimandFragment(apartmentEntity, false))
                } else {
                    GlobalScope.launch(Dispatchers.Main) {
                        var result = true
                        withContext(Dispatchers.IO) {
                            Repository.getRemarkListFromApartment(apartmentEntity.id!!) {
                                val resList = it!!.filter { remark -> remark.isShow }
                                resList.forEach { remark ->
                                    if (remark.isComplete != true) {
                                        result = false
                                    }
                                }
                            }
                        }
                        if (result || apartmentEntity.reviews.size > 1) {
                            activity.initFragment(ResultSecondReviewProgressFragment(apartmentEntity))
                        } else {
                            activity.initFragment(
                                ResultReviewProgressFragment(
                                    apartmentEntity,
                                    isMainList = false,
                                    fromListReview = true
                                )
                            )
                        }
                    }
                }
            else {
                val map = apartmentEntity.progressReviewAll
                activity.model.newApartmentEntity = apartmentEntity
                if (apartmentEntity.typeReview == null) {
                    activity.initFragment(TypeRemontFragment(map))
                } else {
                    if (apartmentEntity.typeApartment == TypeApartment.Parking) {
                        activity.model.backGroundFragment = Pair(
                            activity.application.getDrawable(R.drawable.parking),
                            ContextCompat.getColor(activity.application.baseContext, R.color.colorParkingBlue)
                        )
                        activity.initFragment(
                            ListRemarkFragment(
                                map[AdapterTypeRemont.arrayUnClear.last()]!!,
                                "Парковочное место"
                            )
                        )
                    } else {
                        activity.initFragment(TypeWorkingFragment(map))
                    }
                }
            }
        }
    }
}