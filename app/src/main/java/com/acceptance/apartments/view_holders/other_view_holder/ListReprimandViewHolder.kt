package com.acceptance.apartments.view_holders.other_view_holder

import android.annotation.SuppressLint
import android.content.Context.LAYOUT_INFLATER_SERVICE
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Typeface
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.PopupWindow
import com.acceptance.apartments.MainActivity
import com.acceptance.apartments.R
import com.acceptance.apartments.fragments.other_fragments.ListReprimandFragment
import com.acceptance.apartments.model.DaoRemarkEntity
import com.acceptance.apartments.model.Repository
import com.acceptance.apartments.view_holders.AbstractViewHolder
import kotlinx.android.synthetic.main.component_divider_bold_invisible.view.*
import kotlinx.android.synthetic.main.component_divider_invisible.view.*
import kotlinx.android.synthetic.main.component_pop_up_photo.view.*
import kotlinx.android.synthetic.main.item_reprimand.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class ListReprimandViewHolder(private val view: View, private val isMainList: Boolean = true) :
    AbstractViewHolder(view) {
    private lateinit var fragment: ListReprimandFragment
    private var colorName = 0
    private lateinit var remarkEntity: DaoRemarkEntity

    @SuppressLint("SetTextI18n")
    override fun bind(vararg params: Any) {
        remarkEntity = params[0] as DaoRemarkEntity
        fragment = params[1] as ListReprimandFragment
        val index = getIndexInResource(
            remarkEntity.typeRemark,
            view.resources.getStringArray(R.array.workType)
        )
        colorName = view.resources.getIntArray(R.array.backgroundColorList)[index]
        val iconTitle = view.resources.getStringArray(R.array.iconListInvisible)[index]
        val iconTitleName = iconTitle.split("/")[2].split(".")[0]
        view.iconLayout.setImageDrawable(
            view.resources.getDrawable(
                view.resources.getIdentifier(
                    iconTitleName,
                    "drawable",
                    view.context.packageName
                )
            )
        )
        view.card_layout.setCardBackgroundColor(colorName)

        if (remarkEntity.isComplete != null) {
            if (remarkEntity.isComplete!!) {
                okEnable()
            } else {
                noEnable()
            }
        }
        if (remarkEntity.photo.isNotEmpty()) {
            view.iconPhoto.apply {
                setImageDrawable(view.resources.getDrawable(R.drawable.camera))
                setOnClickListener {
                    val activity = (view.context as MainActivity)
                    val inflater = activity.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater?
                    val popupView = inflater!!.inflate(R.layout.component_pop_up_photo, null)
                    val width = LinearLayout.LayoutParams.MATCH_PARENT
                    val height = LinearLayout.LayoutParams.MATCH_PARENT
                    val options = BitmapFactory.Options()
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888
                    val bitmap = BitmapFactory.decodeFile(remarkEntity.photo, options)
                    popupView.photoReprimand.setImageBitmap(bitmap)
                    val focusable = true
                    val popupWindow = PopupWindow(popupView, width, height, focusable)
                    popupView.setOnClickListener {
                        popupWindow.dismiss()
                    }
                    popupWindow.elevation = 20f
                    popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0)
                }
            }
        } else {
            view.iconPhoto.setImageDrawable(view.resources.getDrawable(R.drawable.not_camera))
        }
        val array = remarkEntity.comment.split("   ")

        view.titleItem.text = remarkEntity.typeRemark
        view.nameItem.text = remarkEntity.nameRemark
        if (array.size > 1) {
            view.nameItem.text = remarkEntity.nameRemark + " " + array[1]
        }
        view.dividerViewCard.setCardBackgroundColor(view.resources.getColor(R.color.white_invisible))
        view.dividerSlim.setBackgroundColor(view.resources.getColor(R.color.white_invisible))
        if (isMainList) {

            if (remarkEntity.isShow) okEnable() else noEnable()

            view.nameItem.setOnClickListener {
                toggle()
            }
            view.quastionItem.text = "Выдать замечание?"
        } else {
            if (remarkEntity.isComplete == true) okEnable() else
                if (remarkEntity.isComplete == false) noEnable()
            view.toggle_view.visibility = View.GONE
        }
        view.descriptionItem.text = remarkEntity.actionIfExist

        view.answerNoItem.setOnClickListener {
            noEnable()
            fragment.updateStateSaveBtn()
            if (!isMainList) updateRemark(false) else updateShowRemark(false)
        }
        view.answerYesItem.setOnClickListener {
            okEnable()
            fragment.updateStateSaveBtn()
            if (!isMainList) updateRemark(true) else updateShowRemark(true)
        }
    }

    private fun updateShowRemark(value: Boolean) = GlobalScope.launch(Dispatchers.Main) {
        withContext(Dispatchers.IO) {
            remarkEntity.isShow = value
            Repository.updateRemark(remarkEntity)
        }
    }

    private fun toggle() {
        view.descriptionItem.visibility =
            if (view.descriptionItem.visibility == View.VISIBLE) View.GONE else View.VISIBLE
        view.dividerView.visibility = view.descriptionItem.visibility
        view.toggle_view.visibility =
            if (view.descriptionItem.visibility == View.GONE) View.VISIBLE else View.GONE
    }

    private fun updateRemark(value: Boolean) = GlobalScope.launch(Dispatchers.Main) {
        withContext(Dispatchers.IO) {
            remarkEntity.isComplete = value
            Repository.updateRemark(remarkEntity)
        }
    }

    companion object {
        fun getIndexInResource(value: String, array: Array<String>): Int {
            array.forEachIndexed { index, s ->
                if (s == value) {
                    return index
                }
            }
            return array.size - 1
        }
    }

    private fun okEnable() {
        view.answerNoItem.setTypeface(view.typeface.typeface, Typeface.NORMAL)
        view.answerYesItem.setTypeface(view.answerYesItem.typeface, Typeface.BOLD)
        view.notCompletePhoto.visibility = View.GONE
        if (isMainList) {
            view.card_layout.setCardBackgroundColor(colorName)
        } else
            view.card_layout.setCardBackgroundColor(view.resources.getColor(R.color.colorPrimary))
    }

    private fun noEnable() {
        view.answerNoItem.setTypeface(view.answerNoItem.typeface, Typeface.BOLD)
        view.answerYesItem.setTypeface(view.typeface.typeface, Typeface.NORMAL)
        view.notCompletePhoto.visibility = View.VISIBLE
        if (isMainList) {
            view.card_layout.setCardBackgroundColor(view.resources.getColor(R.color.colorPrimary))
        } else
            view.card_layout.setCardBackgroundColor(colorName)
    }


}