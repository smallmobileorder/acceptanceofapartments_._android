package com.acceptance.apartments.view_holders.other_view_holder

import android.view.View
import android.widget.LinearLayout
import com.acceptance.apartments.MainActivity
import com.acceptance.apartments.R
import com.acceptance.apartments.fragments.other_fragments.*
import com.acceptance.apartments.view_holders.AbstractViewHolder
import kotlinx.android.synthetic.main.item_menu.view.*

class MenuViewHolder(private val view: View, private val idList: Int = R.array.menu) : AbstractViewHolder(view) {
    override fun bind(vararg params: Any) {
        val text = params[0] as String
        view.itemText.text = text
        view.itemText.post {
            if (text == "Помощь") {
                val layoutParams = view.itemText.layoutParams as LinearLayout.LayoutParams
                layoutParams.topMargin = (layoutParams.topMargin * 2.5).toInt()
                layoutParams.bottomMargin = layoutParams.bottomMargin * 2
                view.itemText.layoutParams = layoutParams
            }
        }
        view.itemText.setOnClickListener {
            var currentText = ""
            val array = view.resources.getStringArray(idList)
            array.forEach {
                if (it == text) {
                    currentText = it
                }
            }
            val activity = (view.context as MainActivity)
            if (idList == R.array.menu_help && currentText == "Назад")
                activity.onBackPressed()
            else
                activity.initFragment(
                    when (idList) {
                        R.array.menu -> when (currentText) {
                            array[0] -> MenuHelpFragment()
                            array[1] -> SourceApartmentFragment()
                            array[2] -> TypeReviewFragment()
                            array[3] -> TypeReviewSecondFragment()
                            array[4] -> HelpFragment(R.raw.help)
                            else -> MenuHelpFragment()
                        }
                        else -> if (currentText == array[6]) MenuFragment()
                        else HelpFragment(
                            when (currentText) {
                                array[0] -> R.raw.main_recomends
                                array[1] -> R.raw.what_you_need_on_review
                                array[2] -> R.raw.process_review
                                array[3] -> R.raw.critical_remark
                                array[4] -> R.raw.garanti_time
                                else -> R.raw.glossary
                            }
                        )
                    }

                )
        }
    }
}