package com.acceptance.apartments

import android.os.Environment
import android.util.Log
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import androidx.test.uiautomator.UiDevice
import com.acceptance.apartments.model.DaoApartmentEntity
import com.acceptance.apartments.model.Repository
import com.acceptance.apartments.view_holders.other_view_holder.MenuViewHolder
import org.junit.Assert.assertEquals
import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters
import java.io.File
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit


/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
@LargeTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)

class ExampleInstrumentedTest {

    private var mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
    @get:Rule
    var activityRule: ActivityTestRule<MainActivity> = ActivityTestRule(MainActivity::class.java)


    @Test
    fun testDisplayOnMenuFragment() {
        //Check display:
        onView(withId(R.id.fragment_menu)).check(matches(isDisplayed()))
    }

    @Test
    fun testClicks() {
        onView(withId(R.id.mainRecycler))
            .perform(actionOnItemAtPosition<MenuViewHolder>(0, click()))
        onView(withId(R.id.fragment_menu_help)).check(matches(isDisplayed()))
        mDevice.pressBack()
        onView(withId(R.id.fragment_menu)).check(matches(isDisplayed()))
        onView(withId(R.id.mainRecycler))
            .perform(actionOnItemAtPosition<MenuViewHolder>(1, click()))
        onView(withId(R.id.fragment_source_given_apartments)).check(matches(isDisplayed()))
        mDevice.pressBack()
        onView(withId(R.id.fragment_menu)).check(matches(isDisplayed()))
        onView(withId(R.id.mainRecycler))
            .perform(actionOnItemAtPosition<MenuViewHolder>(2, click()))
        onView(withId(R.id.fragment_room_for_review)).check(matches(isDisplayed()))
        mDevice.pressBack()
//        takeScreenshot("111")
        onView(withId(R.id.fragment_menu)).check(matches(isDisplayed()))
    }
    private fun takeScreenshot(name: String) {
        val dir = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
            "app_screenshots"
        )
        if (!dir.exists()) {
            if (!dir.mkdirs()) {
                Log.d("Screenshot Test", "Oops! Failed create directory")
            }
        }
        val file = File(dir.path.toString() + File.separator.toString() + name + ".jpg")
        mDevice.takeScreenshot(file)
    }
    @Test
    fun getDateFromDB() {
        // Context of the app under test.
        val signal = CountDownLatch(1)
        Repository.setApartment(DaoApartmentEntity(1111L))
        var result = false

        Repository.getApartmentList { list ->
            result = list.find { it.id == 1111L } != null
        }
        signal.await(3, TimeUnit.SECONDS)
        assertEquals(true, result)

        Repository.clearDB()
        Repository.getApartmentList { list ->
            result = list.isEmpty()
        }
        signal.await(3, TimeUnit.SECONDS)
        assertEquals(true, result)
    }

    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.acceptance.apartments", appContext.packageName)
    }
}
